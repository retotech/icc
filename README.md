# Inter-Cell Communication On Jailhouse
This repository contains a module which was developed to provide ICC (Inter-Cell Communication) capabilities for the PLAMICS (PLatform for MIxed Criticality Systems) project based on the Jailhouse hypervisor.
It consists of a ICC transport layer library (icct), which may be integrated with the IVSHMEM (Inter-Virtual machine SHared MEMory) virtual PCI device provided by the Jailhouse hypervisor.

## Design of the ICC Transport Layer
The ICC transport layer provides means for a client to communicate with a peer ICC transport layer client in another cell of the system. The design is based on the assumption that a lower level physical driver provides means for storing messages to be communicated, as well as notifying that a message is available for reading or has been read and that the storage used for storing the message may be reused. Implementations providing shared memory and an Inter-Core Interrupt with a compainon Inter-Core Interrupt Syndrome Register fulfills these requirements. In the Jailhouse hypervisor the IVSHMEM virtual PCI device is an example of such an implementation.

The ICC Transport Layer has been subdivided into two separate parts, one device dependent driver part, managing the specifics of the device used and another device independent part managing shared memory.

The user of the ICC transport layer, i.e. the ICC transport layer client, has to implement an interface in order for the ICC transport layer library to be able to deliver received messages to the client.

### ICC Transport Layer Client Interface
When initializing the ICC transport layer, a handle to the ICC transport layer client, as specified by the interface `./icct/include/ivshmem_client.h` shall be passed. This interface handle contains client call-back functions:
*  `(*malloc)(...)` - Allocate client accessible/managed memory to which received messages may be copied upon reception by the ICC transport layer.
*  `(*rx_msg)(...)` - Notify the client that a message has been received and copied to memory previously allocated from client managed memory (and may now be read/free:d at will by the client).

### ICC Transport Layer Device Driver Interface
The device specific part of the ICC Transport Layer is specified by the interface defined in `./icct/include/ivshmem_drv.h`. It provides the ICC transport layer client with functions related to the device used for transporting messages between cells:
* `ivshmem_drv_init(...)` - Returns a handle to be used in subsequent calls to the ICC transport layer functions.
* `(*tx_msg)(...)` - Provides the ICC transport layer client with a function to be called for transmitting a message.
* `(*handle_interrupt)(...)` - Function to be called when an interrupt has been received and shall be handled by the ICC Transport Layer.

### ICC Transport Layer Shared Memory Management Interface
The management of shared memory in the ICC transport layer, is specified by the interface defined in `./icct/include/smm.h`. It provides functions used for managing the shared memory used by the ICC transport layer:
* `smm_init(...)` - Returns a handle to the shared memory manager, which shall be passed in all subsequent function calls to the functions provided by shared memory manager.
* `smm_malloc(...)` - Allocate shared memory buffer(s) for message to be transmitted.
* `smm_copy_to_shmem(...)` - Copy client message to shared memory buffer(s) to be transmitted (possibly segmenting the message over several shared memory buffers).
* `smm_free(...)` - Free buffer(s) used for transmitting message.
* `smm_buflen(...)` - Return the length of a received message.
* `smm_copy_from_shmem(...)` - Copy message from shared memory buffer(s) to client managed memory (possibly reassembling messages scattered over several shared memory buffers).

## Implementation of the ICC Transport Layer

The ICC transport layer is implemented by separating OS independent functionality into an ICC transport layer library, referred to as `icct` and located in `./icct/src/`. The Linux dependent ICC transport layer implementation, ICC transport layer, referred to as `icctd`, is located in `./linux/`. The FreeRTOS dependent ICC transport layer implementation is located in `freertos-cell/freertos/Demo/icct/`.

![ICC Components](./doc/components.png)

### The ICC Transport Layer Library

In order for an ICC transport layer client to be able communicate with a peer ICC transport layer client in another cell, both clients have to initialize their respective icct (ICC transport layer library) by calling the `ivshmem_drv.h:ivshmem_drv_init(...)`-function provided by icct.

![icct init sequence](./icct/doc/init_sequence.png)

Once the icct of both clients have been initialized by their respective client, the clients may start transmitting messages to their peer client by calling `tx_msg()` of the icct.

![icct msg trpt sequence](./icct/doc/msg_trpt_sequence.png)

When a message is transmitted (1), shared memory is allocated from the tx-area (2) and the message is copied to shared memory (4) before a tx-interrupt is generated by the transmitting icct to the receiving icct client (6), in order to notify the receiving icct client that a message is available in the rx-area of the receiving icct.

The receiving icct handles the tx-interrupt (8), by fetching the length of the received message (9). It then uses the client call-back function given upon initialization to allocate client memory for the received message (11). The receiving icct then copies the received message from the rx-area of the shared memory to the allocated client memory (13). It then calls the client call-back function indicating that a message has been received and written to the memory just allocated, to the client (15), which handles the message appropriately and frees the memory allocated and used for the message (16). Once the client call-back function returns (17), an rx-interrupt is generated by the receiving icct to the transmitting client to acknowledge that the transmitted message has been received by the receiving icct (18).

The transmitting icct handles the rx-interrupt (20), by freeing the tx-buffer(s) allocated for transmitting the message from the smm (21).

#### Shared Memory Manager

The interface `./icc/icct/include/smm.h` is implemented in `./icc/icct/src/smm.c`, with the assumption that the messages transmitted and received are '\0'-terminated C-style strings.

The tx-area is treated as a circular buffer of `uint8_t`:s. A head-pointer and a tail-pointer keeps track of memory buffers allocated from the tx-area.
* Allocating a tx-buffer consists of updating the tail-pointer into the circular tx-area with the length of the message string, including the terminating '/0'-character.
* Freeing a tx-buffer consists of updating the head-pointer into the circular tx-area until the string terminating '\0'-character is encountered.
* Calculating the length of a buffer consists of counting the number of non string terminating '\0'-characters starting at the given tx-buffer offset + 1.
* Copying to/from the shared tx/rx-area consists of copying the given amount of data, possibly wrapping the circular tx/rx-area end/start from/to a client allocated (linear) memory buffer.

### Implementation of the ICC Transport Layer in Linux

#### ICC Transport Layer Daemon

The Linux ICC transport layer daemon, `icctd`, uses the IVSHMEM user-space IO LKM, `uio_ivshmem.ko`, to access a IVSHMEM virtual PCI device and the ICC transport layer library, `icct`, to transport messages (transmit and receive) to and from a peer cell with which the Linux root-cell shares the IVSHMEM virtual PCI device. `icctd` also provides a Unix domain socket, `/var/run/UDS_icctd`, on which it listens for a client to connect and transport messages to and from a peer cell.

When loading a Linux root-cell configuration containing a valid IVSHMEM PCI device configuration, Jailhouse loads `uio_ivshmem.ko`, which provides user-space IO capabilities to IVSHMEM virtual PCI devices through device files `/dev/uio<ix>`.

`icctd` enables MMIO (Memory Mapped Input/Output) access to the device from user-space by opening a file descriptor to `/dev/uio0` using `open()` and mapping the PCI device register space and the shared memory provided by the IVSHMEM virtual PCI device to user-space using `mmap()`.

Once access to the device from user-space is enabled, `icctd` initializes `icct` with the addresses of the user-space accessible MMIO regions.

When initialization is done, `icctd` goes in to an infinite loop where `poll()` is set up to trig on IVSHMEM_IRQ (`POLLIN` on `/dev/uio0`) from the peer `icct`, a client connects (`POLLIN` on `/var/run/UDS_icctd`), a client wants to transmit a message (`POLLIN` on client socket connection), a client is ready to receive a message (`POLLOUT` on client socket connection).

#### ICC Transport Layer Message Pump

`icctpump`, an utility program pumping messages to and dumping responses from `icctd`, was developed for use during system test and demonstration. It connects to `/var/run/UDS_icctd`, transmits a message using `send()` and receives a messages using `recv()` in a `poll()`-loop. The received message is written to `stdout` and the `poll()`-loop is exited, and the program terminated. 

#### ICC Transport Layer Message Dumper

`icctdump`, an utility program dumping messages from `icctd`, was also developed for use during system test and demonstration. It connects to `/var/run/UDS_icctd`, receives messages using `recv()` in an infinite `poll()`-loop and writes them to `stdout`.

### Implementation of the ICC Transport Layer in FreeRTOS

Currently, no specific ICC transport layer has been implemented in the FreeRTOS inmates. In `freertos-icct-demo`, the FreeRTOS inmate used for demonstrating the ICC transport layer simply transmits messages back as they are received in the `rx_msg()`-callback by calling `tx_msg()`.
In `freertos-demo`, the FreeRTOS inmate used for demonstrator 3, the `segwayTask` periodically (500ms interval) calls `tx_msg()` to publish the speed of the motor.
