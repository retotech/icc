# Testing Plamics ICC
When testing, we distinguish between tests of units and modules and systems.

## Unit Tests
When referring to units, we typically mean objects or libraries, which are not executable by themselves. By linking a unit with another object referencing the symbols of the unit, by passing arguments and checking return values of the function symbols of the unit, we are able to test the behaviour of the unit. We call such an object the unit test harness. The unit test harness may consist of a unit test framework which is linked with unit test cases of a unit test suite into an unit test suite executable. When running the unit test suite executable, the results of the unit test cases may be summarized in a unit test report, which may be processed and presented by a test automation tool.

## Module Tests
When referring to modules, we typically mean a program or executable which interacts with other modules of a system using some well defined application programming interface (API). In order to test such a module we need to communicate with the module using its API and verify that the module behaves as expected by observing the API of the module. In order to automate such API tests we may use a test automation framework, which permits writing test cases and test suites verifying the correct behavoiur of the module and summarizing the results of the API tests into a report, which may be processed and presented by a test automation tool.

## System Tests
A system contains of at least a module, which an actor is able to interact with using some public interface or device. Testing a system incorporates testing all modules of which the system is assembled. This is often referred to as integration testing. System tests may also be automated using some test automation framework, but is usually harder, since it incorporates physical interaction with the system (pushing buttons, watching output, ...). Some system tests often require some manual interaction, hence acheiving full automation is rare.

# Tools Used for Testing Plamics ICC

## Google Test Unit Test Framework
https://github.com/google/googletest/
https://github.com/google/googletest/blob/master/googletest/docs/Documentation.md

### Installing
```bash
cd <download_dir>
wget https://github.com/google/googletest/archive/release-1.8.0.tar.gz
tar xvfz release-1.8.0.tar.gz -C <googletest_dir>
```

## Robot Framework Module Test Framework
https://github.com/robotframework/robotframework
http://robotframework.org/
TBD

### Installing
TBD

### Running
TBD

### Developing Tests
#### RIDE (Robot IDE)
https://github.com/robotframework/RIDE

##### Installation
https://github.com/robotframework/RIDE/wiki/Installation-Instructions
###### On Ubuntu
```bash
sudo apt-get install -y python-wxgtk2.8
sudo pip install robotframework-ride
sudo pip install pygments
```

#### RED (Robot Editor)
Eclipse based editor for RobotFramework testcases
http://nokia.github.io/RED/
http://nokia.github.io/RED/help/
Installed as plugin in Eclipse
##### Install Eclipse (Oxygen) IDE for C/C++ Developers
Download Eclipse from https://eclipse.org and install, either using the Eclipse Installer: https://www.eclipse.org/downloads/download.php?file=/oomph/epp/oxygen/R/eclipse-inst-linux64.tar.gz, or directly by downloading: https://www.eclipse.org/downloads/download.php?file=/technology/epp/downloads/release/oxygen/R/eclipse-cpp-oxygen-R-linux-gtk-x86_64.tar.gz
##### Install RED in Eclipse
Open the Eclipse Marketplace wizard, Help->Eclipse Marketplace, in the Find:-field type "RED" and push Install-button. When the "Review Licenses"-window appears, check "I accept the terms of the license agreement" and push the "Finish"-button.
Once the installation is complete, restart Eclipse.


## Jenkins Automation Server
https://jenkins.io

Used to automate builds, tests, deployment in the Plamics Continuous Integration/Delivery pipeline.

TBD

## Docker Container Platform
https://www.docker.com

Used to control and reproduce the environment of the test automation pipeline

# Tests

## Unit tests

### ICC Transport Layer Library
The functionality of the ICC transport layer library is unit tested using the Google Test unit test framework.
Tests are located in `../icct/test/unit/gtest/`.

#### Building
C.f: https://github.com/google/googletest/tree/master/googletest#incorporating-into-an-existing-cmake-project and http://kaizou.org/2014/11/gtest-cmake/ and https://stackoverflow.com/questions/5998186/adding-command-line-options-to-cmake for details on how the build environment using cmake was set up.

```bash
mkdir ../build/
cd ../build/
cmake .. -Dgtest=ON
make
```

#### Running
```bash
./icct/test/unit/gtest/ivshmem_drv_unittest
./icct/test/unit/gtest/smm_unittest
```

### ICC Session Layer Library
The functionality of the ICC session layer library is unit tested using the Google Test unit test framework.
Tests are located in `../iccs/test/unit/gtest/`.

#### Setting up unit test project
Copied `<googletest_dir>/googletest-release-1.8.0/googletest/make/Makefile` to plamics/icc/iccs/test/unit/gtest/make/` and edited it.

#### Building
```bash
make -C iccs/test/unit/gtest/make/ [clean] [all] GTEST_DIR=<googletest_dir>/googletest-release-1.8.0/googletest
```
#### Running
```bash
iccs/test/unit/gtest/make/conn_mgr_unittest
```

## Module tests

### Module test of Linux ICC Session Layer Daemon
It shall be possible to test ICC Session Layer Daemon on a Linux host without specific hardware or Jailhouse installed using the ICC Transport Layer Simulator; iccts, the ICC Session Layer Daemon; iccsd, and the ICC Session Layer Utilities; iccs-utils (iccspump, iccsdump, ...).

#### Running
Directly from command-line:
```bash
robot --variable ICC-PATH:../ ../linux/test/module/robot/iccsd.robot
```
or in RIDE:
```bash
ride.py ../linux/test/module/robot/iccsd.robot &
```
Then set: Run->Arguments:"--variable ICC-PATH:/path/to/icc", finally hit Start-button.

### Module test of Linux ICC Transport Layer Daemon
TBD

### Module test of FreeRTOS ICC Session Layer Task
TBD

### Module test of FreeRTOS ICC Transport Layer Task
TBD

# Directory structure
`../icct/test/unit/gtest/`

Tests for Plamics ICC units


Unit tests for Plamics ICC units using the Google Test unit test framework

`../linux/test/module/`

Tests for Plamics Linux ICC modules (programs)

`../linux/test/module/robot/`

Test suites and test cases testing Plamics Linux ICC modules using the RobotFramework test automation framework

`../linux/test/module/robot/iccsd.robot`

Test suite testing the Linux ICC Session Layer Daemon (iccsd) module
