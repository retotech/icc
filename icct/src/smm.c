/*
 * ICC, Inter Cell Communication
 *
 * Copyright (c) Retotech AB, 2016-2017
 *
 * Authors:
 *  Jonas Weståker <jonas.westaker@retotech.se>
 *
 * This work is licensed under the terms of the MIT license.  See
 * the COPYING file in the top-level directory.
 */
#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>

struct mem_area {
    uint8_t *base;
    size_t size;
};

typedef struct mem_area mem_area_t;

struct smm {
    mem_area_t *rx;
    mem_area_t *tx;
    uint8_t *tx_head;
    uint8_t *tx_tail;
};

typedef struct smm smm_t;

static mem_area_t tx_area;
static mem_area_t rx_area;
static smm_t smm_area = {
    &rx_area,
    &tx_area,
    NULL,
    NULL,
};

smm_t *
smm_init(uint8_t *tx_base, size_t tx_size, uint8_t *rx_base, size_t rx_size) {
    if ((NULL == tx_base) || (0 >= tx_size) ||
	(NULL == rx_base) || (0 >= rx_size)) {
	return NULL;
    }
    smm_t *smm = &smm_area;
    smm->tx->base = tx_base;
    smm->tx->size = tx_size;
    smm->rx->base = rx_base;
    smm->rx->size = rx_size;
    smm->tx_head = tx_base;
    smm->tx_tail = tx_base;

    return smm;
}

int32_t
smm_malloc(smm_t *smm, size_t len) {
    if (NULL == smm) {
	return -1;
    }

    int32_t offset = smm->tx_tail - smm->tx->base;
    // Allocate from tail of tx area
    if (smm->tx_tail >= smm->tx_head) {
	if (len > ((smm->tx->base + smm->tx->size - smm->tx_tail) +
		   (smm->tx_head - smm->tx->base)))
	{
	    // OOM
	    return -1;
	}
	if (len < smm->tx->base + smm->tx->size - smm->tx_tail) {
	    smm->tx_tail += len;
	} else {
	    // tail wraps
	    smm->tx_tail = smm->tx->base +
		(len - (smm->tx->base + smm->tx->size - smm->tx_tail));
	}
    } else {
	// tail has wrapped
	if (len > (smm->tx_head - smm->tx_tail)) {
	    // OOM
	    return -1;
	}
	smm->tx_tail += len;
    }

    return offset;
}

int32_t
smm_copy_to_shmem(smm_t *smm, int32_t dst_offset,
		  uint8_t *src, size_t len) {
    if (NULL == smm) {
	return -1;
    }
    if (dst_offset >= smm->tx->size) {
	return -1;
    }
    if (NULL == src) {
	return -1;
    }
    if (0 == len) {
	return -1;
    }
    if (smm->tx_tail >= smm->tx_head) {
	if (((smm->tx_head - smm->tx->base) <= dst_offset) &&
	    ((smm->tx_tail - smm->tx->base) >= (dst_offset + len)))
	{
	    // data fits in shmem allocated from tx area
	    memcpy((smm->tx->base + dst_offset), src, len);
	} else {
	    return -1;
	}
    } else {
	// tail has wrapped
	if (dst_offset >= (smm->tx_head - smm->tx->base)) {
	    int32_t head_len = smm->tx->size - dst_offset;
	    if ((dst_offset + len) > smm->tx->size) {
		int32_t tail_len = len - head_len;
		if (smm->tx_tail <=
		    (smm->tx->base + tail_len))
		{
		    memcpy(smm->tx->base,
			   src + head_len,
			   head_len); 
		} else {
		    return -1;
		}
	    }
	    memcpy(smm->tx->base + dst_offset,
		   src,
		   smm->tx->size - dst_offset);
	} else {
	    if (((smm->tx_tail - smm->tx->base) >= dst_offset) &&
		((smm->tx_tail - smm->tx->base) >= dst_offset + len)) 
	    {
		memcpy((smm->tx->base + dst_offset), src, len);
	    } else {
		return -1;
	    }
	}
    }

    return dst_offset;
}

bool
smm_free(smm_t *smm, int32_t offset) {
    if (NULL == smm) {
	return false;
    }
    if (offset != (smm->tx_head - smm->tx->base)) {
	return false;
    }
    if (smm->tx_head == smm->tx_tail) {
	return false;
    }

    while ('\0' != (*(smm->tx_head))) {
	(smm->tx_head)++;
	if (smm->tx_head == (smm->tx->base + smm->tx->size)) {
	    smm->tx_head = smm->tx->base;
	}
    }

    (smm->tx_head)++;
    if (smm->tx_head == (smm->tx->base + smm->tx->size)) {
	smm->tx_head = smm->tx->base;
    }

    return true;
}

ssize_t
smm_buflen(smm_t *smm, int32_t offset) {
    if (NULL == smm) {
	return -1;
    }
    if (offset > smm->rx->size) {
	return -1;
    }
    size_t maxlen = smm->rx->size - offset;
    size_t len = strnlen((char *)smm->rx->base + offset, smm->rx->size - offset);
    if (len == maxlen) {
	len += strnlen((char *)smm->rx->base, smm->rx->size);
    }
    return len + 1;
}

uint8_t *
smm_copy_from_shmem(smm_t *smm, uint8_t *dst,
		    int32_t src_offset, size_t len) {
    if (NULL == smm) {
	return NULL;
    }
    if (NULL == dst) {
	return NULL;
    }
    if (src_offset > smm->rx->size) {
	return NULL;
    }
    if (len > smm->rx->size) {
	return NULL;
    }
    if (src_offset + len > smm->rx->size) {
	size_t head_len = smm->rx->size - src_offset;
	size_t tail_len = src_offset + len - smm->rx->size;
	// tail has wrapped
	memcpy(dst,
	       smm->rx->base + src_offset,
	       head_len);
	memcpy(dst + head_len,
	       smm->rx->base,
	       tail_len);
    } else {
	memcpy(dst, smm->rx->base + src_offset, len);
    }
    return dst;
}
