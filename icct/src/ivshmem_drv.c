/*
 * ICC, Inter Cell Communication
 *
 * Copyright (c) Retotech AB, 2016-2017
 *
 * Authors:
 *  Jonas Weståker <jonas.westaker@retotech.se>
 *
 * This work is licensed under the terms of the MIT license.  See
 * the COPYING file in the top-level directory.
 */
#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#if 0
#include <stdio.h>
#define log(...) printf(__VA_ARGS__)
#else
#define log(...)
#endif

#include "ivshmem_drv.h"
#include "ivshmem_client.h"
#include "smm.h"

#define IVSHMEM_BAR_0_DOORBELL_INTERRUPT_TYPE_MASK (0x80000000)
#define IVSHMEM_BAR_0_DOORBELL_BUFFER_OFFSET_MASK  (~(IVSHMEM_BAR_0_DOORBELL_INTERRUPT_TYPE_MASK))
#define IVSHMEM_BAR_0_DOORBELL_INTERRUPT_TYPE_TX   (0x80000000)
#define IVSHMEM_BAR_0_DOORBELL_INTERRUPT_TYPE_RX   (0x00000000)

struct ivshmem_bar0 {
    uint32_t mask;
    const uint32_t status;
    const uint32_t position;
    const uint32_t doorbell;
    uint32_t lstate;
    const uint32_t rstate;
};

typedef struct ivshmem_bar0 ivshmem_bar0_t;

static bool
generate_tx_interrupt(ivshmem_drv_t *ivshmem_drv, uint32_t offset) {
    // C.f. ivshmem_guest_code/uio/tests/Interrupts/VM/uio_send.c
    // for details on how to generate a tx-interrupt. */
    ivshmem_drv->bar0->lstate =
	IVSHMEM_BAR_0_DOORBELL_INTERRUPT_TYPE_TX | offset;
    log("%s:%d - Tx interrupt generated, ivshmem_drv->bar0:%p lstate:0x%x offset:0x%x\n",
	   __FILE__, __LINE__, ivshmem_drv->bar0, ivshmem_drv->bar0->lstate, offset);
    return true;
}

static bool
generate_rx_interrupt(ivshmem_drv_t *ivshmem_drv, uint32_t offset) {
    ivshmem_drv->bar0->lstate =
	IVSHMEM_BAR_0_DOORBELL_INTERRUPT_TYPE_RX | offset;
    log("%s:%d - Rx interrupt generated, ivshmem_drv->bar0:%p lstate:0x%x offset:0x%x\n",
	   __FILE__, __LINE__, ivshmem_drv->bar0, ivshmem_drv->bar0->lstate, offset);
    return true;
}

static bool
tx_ack(ivshmem_drv_t *ivshmem_drv, uint32_t offset) {
    if (NULL == ivshmem_drv) {
	return false;
    }
    return smm_free(ivshmem_drv->smm, offset);
}

static bool
rx_msg(ivshmem_drv_t *ivshmem_drv, uint32_t offset) {
    log("%s:%d - %s, offset:0x%x\n", __FILE__, __LINE__, __func__, offset);
    if (NULL == ivshmem_drv) {
	return false;
    }
    log("%s:%d - %s\n", __FILE__, __LINE__, __func__);
    if (NULL == ivshmem_drv->client) {
	return false;
    }
    log("%s:%d - %s\n", __FILE__, __LINE__, __func__);
    if (NULL == ivshmem_drv->client->malloc) {
	return false;
    }
    log("%s:%d - %s\n", __FILE__, __LINE__, __func__);
    if (NULL == ivshmem_drv->client->rx_msg) {
	return false;
    }
    log("%s:%d - %s\n", __FILE__, __LINE__, __func__);
    ssize_t len = smm_buflen(ivshmem_drv->smm, offset);
    if (0 > len) {
	return false;
    }
    log("%s:%d - %s, len:%ld\n", __FILE__, __LINE__, __func__, len);
    uint8_t *msg = ivshmem_drv->client->malloc(ivshmem_drv->client, len);
    if (NULL == msg) {
	log("%s:%d - %s\n", __FILE__, __LINE__, __func__);
	return false;
    }
    log("%s:%d - %s\n", __FILE__, __LINE__, __func__);
    if (msg != smm_copy_from_shmem(ivshmem_drv->smm, msg, offset, len)) {
	// FIXME: free buffer allocated by client...
	return false;
    }
    log("%s:%d - %s\n", __FILE__, __LINE__, __func__);
    ivshmem_drv->client->rx_msg(ivshmem_drv->client, msg, len);
    log("%s:%d - %s\n", __FILE__, __LINE__, __func__);
    return generate_rx_interrupt(ivshmem_drv, offset);
}

static bool
tx_msg(ivshmem_drv_t *ivshmem_drv, uint8_t *msg, size_t len) {
    if (NULL == ivshmem_drv) {
	return false;
    }
    if (NULL == msg) {
        return false;
    }
    if (0 == len) {
        return false;
    }

    int32_t offset = smm_malloc(ivshmem_drv->smm, len);
    if (0 > offset) {
	return false;
    }
    smm_copy_to_shmem(ivshmem_drv->smm, offset, msg, len);
    generate_tx_interrupt(ivshmem_drv, offset);

    return true;
}

static bool
handle_interrupt(ivshmem_drv_t *ivshmem_drv) {
    bool status = false;

    if (NULL == ivshmem_drv) {
	return status;
    }

    uint32_t rstate = ivshmem_drv->bar0->rstate;

    log("%s:%d - Handling interrupt, ivshmem_drv->bar0:%p rstate:0x%x\n",
	__FILE__, __LINE__, ivshmem_drv->bar0, rstate);

    uint32_t type = rstate & IVSHMEM_BAR_0_DOORBELL_INTERRUPT_TYPE_MASK;
    uint32_t offset = rstate & IVSHMEM_BAR_0_DOORBELL_BUFFER_OFFSET_MASK;

    switch (type) {
    case IVSHMEM_BAR_0_DOORBELL_INTERRUPT_TYPE_TX: {
	// Read message transmitted by peer to ivshmem rx area,
	// call client malloc() and rx_msg()
	// then generate rx interrupt to peer, indicating that the
	// message has been read and its ivshmem resources may be free:d/reused
	log("%s:%d - Handling Tx interrupt, offset:0x%x\n",
	    __FILE__, __LINE__, offset);
	status = rx_msg(ivshmem_drv, offset);
	break;
    }
    case IVSHMEM_BAR_0_DOORBELL_INTERRUPT_TYPE_RX:
	// Free/reuse the ivshmem resorces in ivshmem tx area
	// used to write a message to peer in tx_msg()
	log("%s:%d - Handling Rx interrupt, offset:0x%x\n",
	    __FILE__, __LINE__, offset);
	status = tx_ack(ivshmem_drv, offset);
	break;
    default:
	break;
    }
    return status;
}

static ivshmem_drv_t ivshmem_drv = {
    NULL,
    NULL,
    NULL,
    tx_msg,
    handle_interrupt
};

ivshmem_drv_t *
ivshmem_drv_init(ivshmem_client_t *client,
		 uint8_t *bar0_base, size_t bar0_size,
		 uint8_t *tx_base, size_t tx_size,
		 uint8_t *rx_base, size_t rx_size)
{
    if ((NULL == client) ||
	(NULL == bar0_base) || (0 == bar0_size) ||
	(NULL == tx_base) || (0 == tx_size) ||
	(NULL == rx_base) || (0 == rx_size))
    {
        return NULL;
    }
    if (NULL == client->malloc) {
	return NULL;
    }
    if (NULL == client->rx_msg) {
	return NULL;
    }

    ivshmem_drv.client = client;
    ivshmem_drv.bar0 = (ivshmem_bar0_t *)bar0_base;
    ivshmem_drv.smm = smm_init(tx_base, tx_size, rx_base, rx_size);
    return &ivshmem_drv;
}
