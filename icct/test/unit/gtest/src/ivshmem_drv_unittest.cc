#include <gtest/gtest.h>

#include "ivshmem_client.h"
#include "ivshmem_drv.h"

static bool called = false;
static uint8_t *rx_msg = NULL;

static uint8_t *
dummy_client_malloc(ivshmem_client_t *ivshmem_client, size_t len) {
    printf("%s(%p, %lu)\n", __func__, ivshmem_client, len);
    return (uint8_t *)malloc(len);
}

static void
dummy_client_rx_msg(ivshmem_client_t *ivshmem_client, uint8_t *msg, size_t len) {
    printf("%s(%p, %s, %lu)\n", __func__, ivshmem_client, msg, len);
    called = true;
    rx_msg = (uint8_t *)malloc(len);
    memcpy(rx_msg, msg, len);
    return;
}

static bool
dummy_client_rx_msg_called() {
    return called;
}

static ivshmem_client_t ivshmem_client = {
    dummy_client_malloc,
    dummy_client_rx_msg
};

TEST(TestIvshmemDrvInit, InvalidArguments) {
    ivshmem_drv_t *dd = ivshmem_drv_init(NULL, NULL, 0, NULL, 0, NULL, 0);
    EXPECT_TRUE(dd == NULL);
}

TEST(TestIvshmemDrvInit, ClientArgumentNull) {
    uint8_t bar[10];
    uint8_t tx[10];
    uint8_t rx[10];
    ivshmem_drv_t *dd =
	ivshmem_drv_init(NULL,
			 &(bar[0]), sizeof(bar)/sizeof(bar[0]),
			 &(tx[0]), sizeof(tx)/sizeof(tx[0]),
			 &(rx[0]), sizeof(rx)/sizeof(rx[0]));
    EXPECT_TRUE(dd == NULL);
}

TEST(TestIvshmemDrvInit, ClientAllocMsgFuncNull) {
    uint8_t bar[10];
    uint8_t tx[10];
    uint8_t rx[10];
    ivshmem_client_t client = {
	NULL,
	dummy_client_rx_msg
    };
    EXPECT_TRUE(NULL ==
		ivshmem_drv_init(&client,
				 &(bar[0]), sizeof(bar)/sizeof(bar[0]),
				 &(tx[0]), sizeof(tx)/sizeof(tx[0]),
				 &(rx[0]), sizeof(rx)/sizeof(rx[0])));
}

TEST(TestIvshmemDrvInit, ClientRxMsgFuncNull) {
    uint8_t bar[10];
    uint8_t tx[10];
    uint8_t rx[10];
    ivshmem_client_t client = {
	dummy_client_malloc,
	NULL
    };
    EXPECT_TRUE(NULL ==
		ivshmem_drv_init(&client,
				 &(bar[0]), sizeof(bar)/sizeof(bar[0]),
				 &(tx[0]), sizeof(tx)/sizeof(tx[0]),
				 &(rx[0]), sizeof(rx)/sizeof(rx[0])));
}

TEST(TestIvshmemDrvInit, BarArgumentNull) {
    uint8_t bar[10];
    uint8_t tx[10];
    uint8_t rx[10];
    ivshmem_drv_t *dd =
	ivshmem_drv_init(&ivshmem_client,
			 NULL, sizeof(bar)/sizeof(bar[0]),
			 &(tx[0]), sizeof(tx)/sizeof(tx[0]),
			 &(rx[0]), sizeof(rx)/sizeof(rx[0]));
    EXPECT_TRUE(dd == NULL);
}

TEST(TestIvshmemDrvInit, BarSizeArgumentZero) {
    uint8_t bar[10];
    uint8_t tx[10];
    uint8_t rx[10];
    ivshmem_drv_t *dd =
	ivshmem_drv_init(&ivshmem_client,
			 &(bar[0]), 0,
			 &(tx[0]), sizeof(tx)/sizeof(tx[0]),
			 &(rx[0]), sizeof(rx)/sizeof(rx[0]));
    EXPECT_TRUE(dd == NULL);
}

TEST(TestIvshmemDrvInit, TxBaseArgumentNull) {
    uint8_t bar[10];
    uint8_t tx[10];
    uint8_t rx[10];
    ivshmem_drv_t *dd =
	ivshmem_drv_init(&ivshmem_client,
			 &(bar[0]), sizeof(bar)/sizeof(bar[0]),
			 NULL, sizeof(tx)/sizeof(tx[0]),
			 &(rx[0]), sizeof(rx)/sizeof(rx[0]));
    EXPECT_TRUE(dd == NULL);
}

TEST(TestIvshmemDrvInit, TxSizeArgumentZero) {
    uint8_t bar[10];
    uint8_t tx[10];
    uint8_t rx[10];
    ivshmem_drv_t *dd =
	ivshmem_drv_init(&ivshmem_client,
			 &(bar[0]), sizeof(bar)/sizeof(bar[0]),
			 &(tx[0]), 0,
			 &(rx[0]), sizeof(rx)/sizeof(rx[0]));
    EXPECT_TRUE(dd == NULL);
}

TEST(TestIvshmemDrvInit, RxBaseArgumentNull) {
    uint8_t bar[10];
    uint8_t tx[10];
    uint8_t rx[10];
    ivshmem_drv_t *dd =
	ivshmem_drv_init(&ivshmem_client,
			 &(bar[0]), sizeof(bar)/sizeof(bar[0]),
			 &(tx[0]), sizeof(tx)/sizeof(tx[0]),
			 &(rx[0]), 0);
    EXPECT_TRUE(dd == NULL);
}

TEST(TestIvshmemDrvInit, RxSizeArgumentZero) {
    uint8_t bar[10];
    uint8_t tx[10];
    uint8_t rx[10];
    ivshmem_drv_t *dd =
	ivshmem_drv_init(&ivshmem_client,
			 &(bar[0]), sizeof(bar)/sizeof(bar[0]),
			 &(tx[0]), sizeof(tx)/sizeof(tx[0]),
			 NULL, sizeof(rx)/sizeof(rx[0]));
    EXPECT_TRUE(dd == NULL);
}

TEST(TestIvshmemDrvTxMsg, DataArgumentIsNull) {
    uint8_t bar[10];
    uint8_t tx[10];
    uint8_t rx[10];
    ivshmem_drv_t *dd =
	ivshmem_drv_init(&ivshmem_client,
			 &(bar[0]), sizeof(bar)/sizeof(bar[0]),
			 &(tx[0]), sizeof(tx)/sizeof(tx[0]),
			 &(rx[0]), sizeof(rx)/sizeof(rx[0]));
    ASSERT_FALSE(dd == NULL);
    char str[] = "Hello world\n";
    //uint8_t *data = (uint8_t *)str;
    size_t len = strlen(str) + 1;
    EXPECT_FALSE(dd->tx_msg(dd, NULL, len));
}

TEST(TestIvshmemDrvTxMsg, IvshmemDrvArgumentIsNull) {
    uint8_t bar[10];
    uint8_t tx[10];
    uint8_t rx[10];
    ivshmem_drv_t *dd =
	ivshmem_drv_init(&ivshmem_client,
			 &(bar[0]), sizeof(bar)/sizeof(bar[0]),
			 &(tx[0]), sizeof(tx)/sizeof(tx[0]),
			 &(rx[0]), sizeof(rx)/sizeof(rx[0]));
    ASSERT_FALSE(dd == NULL);
    char str[] = "Hello world\n";
    uint8_t *data = (uint8_t *)str;
    size_t len = strlen(str) + 1;
    EXPECT_FALSE(dd->tx_msg(NULL, data, len));
}

TEST(TestIvshmemDrvTxMsg, LenArgumentIsZero) {
    uint8_t bar[10];
    uint8_t tx[10];
    uint8_t rx[10];
    ivshmem_drv_t *dd =
	ivshmem_drv_init(&ivshmem_client,
			 &(bar[0]), sizeof(bar)/sizeof(bar[0]),
			 &(tx[0]), sizeof(tx)/sizeof(tx[0]),
			 &(rx[0]), sizeof(rx)/sizeof(rx[0]));
    ASSERT_FALSE(dd == NULL);
    char str[] = "Hello world\n";
    uint8_t *data = (uint8_t *)str;
    //size_t len = strlen(str) + 1;
    EXPECT_FALSE(dd->tx_msg(dd, data, 0));
}

TEST(TestIvshmemDrvTxMsg, TxMsgLenLessThanTxArea) {
    char str[] = "Hello ivshmem world!!!\n";
    uint8_t *data = (uint8_t *)str;
    size_t len = strlen(str) + 1; // Include string termination character ('\0')
    uint8_t bar[10];
    uint8_t tx[len + 1];
    uint8_t rx[10];
    ivshmem_drv_t *dd =
	ivshmem_drv_init(&ivshmem_client,
			 &(bar[0]), sizeof(bar)/sizeof(bar[0]),
			 &(tx[0]), sizeof(tx)/sizeof(tx[0]),
			 &(rx[0]), sizeof(rx)/sizeof(rx[0]));
    ASSERT_FALSE(dd == NULL);
    EXPECT_TRUE(dd->tx_msg(dd, data, len));
    //uint8_t *tx_buf = dd->tx_msg(dd, data, len);
    //EXPECT_FALSE(NULL == tx_buf);
    //EXPECT_EQ(tx_buf, &(tx[0]));
    //EXPECT_EQ(0, strncmp((char *)tx, str, len));
}

TEST(TestIvshmemDrvTxMsg, TxMsgLenEqualToTxArea) {
    char str[] = "Hello ivshmem world!!!\n";
    uint8_t *data = (uint8_t *)str;
    size_t len = strlen(str) + 1; // Include string termination character ('\0')
    uint8_t bar[10];
    uint8_t tx[len];
    uint8_t rx[10];
    ivshmem_drv_t *dd =
	ivshmem_drv_init(&ivshmem_client,
			 &(bar[0]), sizeof(bar)/sizeof(bar[0]),
			 &(tx[0]), sizeof(tx)/sizeof(tx[0]),
			 &(rx[0]), sizeof(rx)/sizeof(rx[0]));
    ASSERT_FALSE(dd == NULL);
    EXPECT_TRUE(dd->tx_msg(dd, data, len));
    //uint8_t *tx_buf = dd->tx_msg(dd, data, len);
    //EXPECT_FALSE(NULL == tx_buf);
    //EXPECT_EQ(tx_buf, &(tx[0]));
    //EXPECT_EQ(0, strncmp((char *)tx, str, len));
}

TEST(TestIvshmemDrvTxMsg, TxMsgLenLargerThanTxArea) {
    char str[] = "Hello ivshmem world!!!\n";
    uint8_t *data = (uint8_t *)str;
    size_t len = strlen(str) + 1;
    uint8_t bar[10];
    uint8_t tx[len -1];
    uint8_t rx[10];
    ivshmem_drv_t *dd =
	ivshmem_drv_init(&ivshmem_client,
			 &(bar[0]), sizeof(bar)/sizeof(bar[0]),
			 &(tx[0]), sizeof(tx)/sizeof(tx[0]),
			 &(rx[0]), sizeof(rx)/sizeof(rx[0]));
    ASSERT_FALSE(dd == NULL);
    EXPECT_FALSE(dd->tx_msg(dd, data, len));
    //uint8_t *tx_buf = dd->tx_msg(dd, data, len);
    //EXPECT_EQ(NULL, tx_buf);
}

TEST(TestIvshmemDrvHndInt, IvshmemDrvArgIsNull) {
    uint8_t bar[10];
    uint8_t tx[10];
    uint8_t rx[10];
    ivshmem_drv_t *dd =
	ivshmem_drv_init(&ivshmem_client,
			 &(bar[0]), sizeof(bar)/sizeof(bar[0]),
			 &(tx[0]), sizeof(tx)/sizeof(tx[0]),
			 &(rx[0]), sizeof(rx)/sizeof(rx[0]));
    ASSERT_FALSE(dd == NULL);
    EXPECT_FALSE(dd->handle_interrupt(NULL));
}

TEST(TestIvshmemDrvHndInt, GenerateTxInterrupt) {
    uint32_t bar[7]; // ivshmem_drv.c:struct ivshmem_bar0
    bar[5] = 0x80000006UL; // ivshmem_drv.c:IVSHMEM_BAR_0_DOORBELL_INTERRUPT_TYPE_TX
    uint8_t tx[10] = "Hello";
    uint8_t rx[20] = "Hello from Tx";
    ivshmem_drv_t *dd =
	ivshmem_drv_init(&ivshmem_client,
			 (uint8_t *)(&(bar[0])), sizeof(bar),
			 &(tx[0]), sizeof(tx)/sizeof(tx[0]),
			 &(rx[0]), sizeof(rx)/sizeof(rx[0]));
    ASSERT_FALSE(dd == NULL);
    EXPECT_FALSE(dummy_client_rx_msg_called());
    EXPECT_TRUE(dd->handle_interrupt(dd));
    EXPECT_TRUE(dummy_client_rx_msg_called());
}
