#include <gtest/gtest.h>

#include "smm.h"

// TEST(TestSmmInit, ...) {
//     ...
// }

TEST(TestSmmInit, InvalidArgs) {
    smm_t *smm = smm_init(NULL, 0, NULL, 0);
    EXPECT_TRUE(NULL == smm);
}

/* TODO: Rx and Tx areas overlap. */

TEST(TestSmmInit, ValidArguments) {
    uint8_t tx[1];
    uint8_t rx[1];
    smm_t *smm = smm_init(tx, sizeof(tx)/sizeof(tx[0]),
			  rx, sizeof(rx)/sizeof(rx[0]));
    EXPECT_FALSE(NULL == smm);
}

// TEST(TestSmmMalloc, ...) {
//     ...
// }

TEST(TestSmmMalloc, SmmArgIsNull) {
    EXPECT_EQ(-1, smm_malloc(NULL, 1));
}

TEST(TestSmmMalloc, LenArgSmallerThanZero) {
    uint8_t tx[2];
    uint8_t rx[1];
    smm_t *smm = smm_init(tx, sizeof(tx)/sizeof(tx[0]),
			  rx, sizeof(rx)/sizeof(rx[0]));
    EXPECT_EQ(-1, smm_malloc(smm, -1));
}

TEST(TestSmmMalloc, LenArgSmallerThanShmemTxArea) {
    uint8_t tx[2];
    uint8_t rx[1];
    smm_t *smm = smm_init(tx, sizeof(tx)/sizeof(tx[0]),
			  rx, sizeof(rx)/sizeof(rx[0]));
    EXPECT_EQ(0, smm_malloc(smm, (sizeof(tx)/sizeof(tx[0])) - 1));
}

TEST(TestSmmMalloc, LenArgEqualToShmemTxArea) {
    uint8_t tx[1];
    uint8_t rx[1];
    smm_t *smm = smm_init(tx, sizeof(tx)/sizeof(tx[0]),
			  rx, sizeof(rx)/sizeof(rx[0]));
    EXPECT_EQ(0, smm_malloc(smm, (sizeof(tx)/sizeof(tx[0]))));
}

TEST(TestSmmMalloc, LenArgLargerThanShmemTxArea) {
    uint8_t tx[1];
    uint8_t rx[1];
    smm_t *smm = smm_init(tx, sizeof(tx)/sizeof(tx[0]),
			  rx, sizeof(rx)/sizeof(rx[0]));
    EXPECT_EQ(-1, smm_malloc(smm, (sizeof(tx)/sizeof(tx[0])) + 1));
}

// TEST(TestSmmCopyToShmem, ...) {
//     ...
// }

TEST(TestSmmCopyToShmem, SmmArgIsNull) {
    char str[] = "Hello";
    EXPECT_EQ(-1, smm_copy_to_shmem(NULL, 0, (uint8_t *)str, 0));
}

TEST(TestSmmCopyToShmem, DstOffsetArgLargerThanTxAreaSize) {
    uint8_t tx[10];
    uint8_t rx[1];
    smm_t *smm = smm_init(tx, sizeof(tx)/sizeof(tx[0]),
			  rx, sizeof(rx)/sizeof(rx[0]));
    char str[] = "Hello";
    EXPECT_EQ(-1, smm_copy_to_shmem(smm, sizeof(tx)/sizeof(tx[0]),
				    (uint8_t *)str, strlen(str) + 1));
}

TEST(TestSmmCopyToShmem, DstOffsetArgIsNotAllocated) {
    uint8_t tx[10];
    uint8_t rx[1];
    smm_t *smm = smm_init(tx, sizeof(tx)/sizeof(tx[0]),
			  rx, sizeof(rx)/sizeof(rx[0]));
    char str[] = "Hello";
    EXPECT_EQ(-1, smm_copy_to_shmem(smm, 0, (uint8_t *)str, strlen(str) + 1));
}

TEST(TestSmmCopyToShmem, SrcArgIsNull) {
    uint8_t tx[10];
    uint8_t rx[1];
    smm_t *smm = smm_init(tx, sizeof(tx)/sizeof(tx[0]),
			  rx, sizeof(rx)/sizeof(rx[0]));
    char str[] = "Hello";
    int32_t offset;
    EXPECT_EQ(0, offset = smm_malloc(smm, strlen(str) + 1));
    EXPECT_EQ(-1, smm_copy_to_shmem(smm, offset, NULL, strlen(str) + 1));
}

TEST(TestSmmCopyToShmem, LenArgIsZero) {
    uint8_t tx[10];
    uint8_t rx[1];
    smm_t *smm = smm_init(tx, sizeof(tx)/sizeof(tx[0]),
			  rx, sizeof(rx)/sizeof(rx[0]));
    char str[] = "Hello";
    int32_t offset;
    EXPECT_EQ(0, offset = smm_malloc(smm, strlen(str) + 1));
    EXPECT_EQ(-1, smm_copy_to_shmem(smm, offset, (uint8_t *)str, 0));
}

TEST(TestSmmCopyToShmem, LenArgSmallerThanBufLen) {
    char str[] = "Hello";
    uint8_t tx[10];
    uint8_t rx[1];
    smm_t *smm = smm_init(tx, sizeof(tx)/sizeof(tx[0]),
			  rx, sizeof(rx)/sizeof(rx[0]));
    int32_t offset;
    size_t len = strlen(str) + 1;
    EXPECT_EQ(0, offset = smm_malloc(smm, len));
    EXPECT_EQ(offset, smm_copy_to_shmem(smm, offset, (uint8_t *)str, len - 1));
}

TEST(TestSmmCopyToShmem, LenArgEqualsBufLen) {
    char str[] = "Hello";
    uint8_t tx[10];
    uint8_t rx[1];
    smm_t *smm = smm_init(tx, sizeof(tx)/sizeof(tx[0]),
			  rx, sizeof(rx)/sizeof(rx[0]));
    int32_t offset;
    size_t len = strlen(str) + 1;
    EXPECT_EQ(0, offset = smm_malloc(smm, len));
    EXPECT_EQ(offset, smm_copy_to_shmem(smm, offset, (uint8_t *)str, len));
}

TEST(TestSmmCopyToShmem, LenArgGreaterThanBufLen) {
    char str[] = "Hello";
    uint8_t tx[10];
    uint8_t rx[1];
    smm_t *smm = smm_init(tx, sizeof(tx)/sizeof(tx[0]),
			  rx, sizeof(rx)/sizeof(rx[0]));
    size_t len = strlen(str) + 1;
    int32_t offset;
    EXPECT_EQ(0, offset = smm_malloc(smm, len));
    EXPECT_EQ(-1, smm_copy_to_shmem(smm, offset, (uint8_t *)str, len + 1));
}

TEST(TestSmmCopyToShmem, DstBufWaps) {
    char str[] = "Hello";
    uint8_t tx[10];
    uint8_t rx[1];
    smm_t *smm = smm_init(tx, sizeof(tx)/sizeof(tx[0]),
			  rx, sizeof(rx)/sizeof(rx[0]));
    size_t len = strlen(str) + 1;
    int32_t offset;
    EXPECT_EQ(0, offset = smm_malloc(smm, len));
    EXPECT_EQ(offset, smm_copy_to_shmem(smm, offset, (uint8_t *)str, len));
    EXPECT_TRUE(smm_free(smm, offset));
    EXPECT_EQ((int32_t)len, offset = smm_malloc(smm, len));
    EXPECT_EQ(offset, smm_copy_to_shmem(smm, offset, (uint8_t *)str, len));
    EXPECT_EQ(0, strcmp((char *)tx, "o"));
}

// TEST(TestSmmFree, ...) {
//     ...
// }

TEST(TestSmmFree, SmmArgIsNull) {
    EXPECT_FALSE(smm_free(NULL, 0));
}

TEST(TestSmmFree, OffsetArgOutsideTxArea) {
    uint8_t tx[10];
    uint8_t rx[1];
    smm_t *smm = smm_init(tx, sizeof(tx)/sizeof(tx[0]),
			  rx, sizeof(rx)/sizeof(rx[0]));
    EXPECT_FALSE(smm_free(smm, 10));
}

TEST(TestSmmFree, OffsetArgNonAllocated) {
    uint8_t tx[10];
    uint8_t rx[1];
    smm_t *smm = smm_init(tx, sizeof(tx)/sizeof(tx[0]),
			  rx, sizeof(rx)/sizeof(rx[0]));
    EXPECT_FALSE(smm_free(smm, 0));
}

TEST(TestSmmFree, OffsetArgStartsAtFirstAllocated) {
    char str[] = "Hello";
    uint8_t tx[10];
    uint8_t rx[1];
    smm_t *smm = smm_init(tx, sizeof(tx)/sizeof(tx[0]),
			  rx, sizeof(rx)/sizeof(rx[0]));
    int32_t offset;
    size_t len = strlen(str) + 1;
    EXPECT_EQ(0, offset = smm_malloc(smm, len));
    EXPECT_EQ(offset, smm_copy_to_shmem(smm, offset, (uint8_t *)str, len));
    EXPECT_TRUE(smm_free(smm, offset));
}

TEST(TestSmmFree, OffsetArgInFirstAllocated) {
    char str[] = "Hello";
    uint8_t tx[10];
    uint8_t rx[1];
    smm_t *smm = smm_init(tx, sizeof(tx)/sizeof(tx[0]),
			  rx, sizeof(rx)/sizeof(rx[0]));
    int32_t offset;
    size_t len = strlen(str) + 1;
    EXPECT_EQ(0, offset = smm_malloc(smm, len));
    EXPECT_EQ(offset, smm_copy_to_shmem(smm, offset, (uint8_t *)str, len));
    EXPECT_FALSE(smm_free(smm, offset + len - 1));
}

TEST(TestSmmFree, OffsetArgPastFirstAllocated) {
    char str[] = "Hello";
    uint8_t tx[10];
    uint8_t rx[1];
    smm_t *smm = smm_init(tx, sizeof(tx)/sizeof(tx[0]),
			  rx, sizeof(rx)/sizeof(rx[0]));
    int32_t offset;
    size_t len = strlen(str) + 1;
    EXPECT_EQ(0, offset = smm_malloc(smm, len));
    EXPECT_EQ(offset, smm_copy_to_shmem(smm, offset, (uint8_t *)str, len));
    EXPECT_FALSE(smm_free(smm, offset + len));
}

// TEST(TestSmmBuflen, ...) {
//     ...
// }

TEST(TestSmmBuflen, SmmArgIsNull) {
    EXPECT_EQ(-1, smm_buflen(NULL, 0));
}

TEST(TestSmmBuflen, OffsetArgLargerThanRxAreaSize) {
    uint8_t tx[1];
    uint8_t rx[1];
    smm_t *smm = smm_init(tx, sizeof(tx)/sizeof(tx[0]),
			  rx, sizeof(rx)/sizeof(rx[0]));
    EXPECT_EQ(-1, smm_buflen(smm, sizeof(rx)/sizeof(rx[0]) + 1));
}

TEST(TestSmmBuflen, OffsetArgAtAllocatedBuffer) {
    uint8_t tx[10];
    //uint8_t rx[10];
    // Tx and Rx areas are the same!!!
    smm_t *smm = smm_init(tx, sizeof(tx)/sizeof(tx[0]),
			  tx, sizeof(tx)/sizeof(tx[0]));
    int32_t offset;
    char str[] = "Hello";
    size_t len = strlen(str) + 1;
    EXPECT_EQ(0, offset = smm_malloc(smm, len));
    EXPECT_EQ(offset, smm_copy_to_shmem(smm, offset, (uint8_t *)str, len));
    EXPECT_EQ((ssize_t)len, smm_buflen(smm, offset));
}

TEST(TestSmmBuflen, OffsetArgInsideAllocatedBuffer) {
    uint8_t tx[10];
    //uint8_t rx[10];
    // Tx and Rx areas are the same!!!
    smm_t *smm = smm_init(tx, sizeof(tx)/sizeof(tx[0]),
			  tx, sizeof(tx)/sizeof(tx[0]));
    int32_t offset;
    char str[] = "Hello";
    size_t len = strlen(str) + 1;
    EXPECT_EQ(0, offset = smm_malloc(smm, len));
    EXPECT_EQ(offset, smm_copy_to_shmem(smm, offset, (uint8_t *)str, len));
    EXPECT_EQ((ssize_t)len - 1, smm_buflen(smm, offset + 1));
}

TEST(TestSmmBuflen, OffsetArgOutsideAllocatedBuffer) {
    uint8_t tx[10];
    //uint8_t rx[10];
    // Tx and Rx areas are the same!!!
    smm_t *smm = smm_init(tx, sizeof(tx)/sizeof(tx[0]),
			  tx, sizeof(tx)/sizeof(tx[0]));
    int32_t offset;
    char str[] = "Hello";
    size_t len = strlen(str) + 1;
    EXPECT_EQ(0, offset = smm_malloc(smm, len));
    EXPECT_EQ(offset, smm_copy_to_shmem(smm, offset, (uint8_t *)str, len));
    EXPECT_EQ(1, smm_buflen(smm, offset + len));
}

TEST(TestSmmBuflen, AllocatedBufferWraps) {
    uint8_t tx[10];
    //uint8_t rx[10];
    // Tx and Rx areas are the same!!!
    smm_t *smm = smm_init(tx, sizeof(tx)/sizeof(tx[0]),
			  tx, sizeof(tx)/sizeof(tx[0]));
    int32_t offset;
    char str[] = "Hello";
    size_t len = strlen(str) + 1;
    EXPECT_EQ(0, offset = smm_malloc(smm, len));
    EXPECT_EQ(offset, smm_copy_to_shmem(smm, offset, (uint8_t *)str, len));
    //EXPECT_EQ(1, smm_buflen(smm, offset + len));
    EXPECT_TRUE(smm_free(smm, offset));
    EXPECT_EQ((ssize_t)len, offset = smm_malloc(smm, len));
    EXPECT_EQ(offset, smm_copy_to_shmem(smm, offset, (uint8_t *)str, len));
    EXPECT_EQ((ssize_t)len, smm_buflen(smm, offset));
}

// TEST(TestSmmCopyFromShmem, ...) {
//     ...
// }

TEST(TestSmmCopyFromShmem, SmmArgIsNull) {
    EXPECT_TRUE(NULL == smm_copy_from_shmem(NULL, NULL, 0, 0));
}

TEST(TestSmmCopyFromShmem, DstArgIsNull) {
    uint8_t tx[10];
    //uint8_t rx[10];
    // Tx and Rx areas are the same!!!
    smm_t *smm = smm_init(tx, sizeof(tx)/sizeof(tx[0]),
			  tx, sizeof(tx)/sizeof(tx[0]));
    EXPECT_TRUE(NULL == smm_copy_from_shmem(smm, NULL, 0, 0));
}

TEST(TestSmmCopyFromShmem, SrcOffsetArgIsOutsideRxArea) {
    uint8_t tx[10];
    //uint8_t rx[10];
    // Tx and Rx areas are the same!!!
    uint8_t *dst = NULL;
    smm_t *smm = smm_init(tx, sizeof(tx)/sizeof(tx[0]),
			  tx, sizeof(tx)/sizeof(tx[0]));
    EXPECT_TRUE(NULL ==
		smm_copy_from_shmem(smm, dst,
				    sizeof(tx)/sizeof(tx[0]) + 1, 0));
}

TEST(TestSmmCopyFromShmem, LenArgGreaterThanRxArea) {
    uint8_t tx[10];
    //uint8_t rx[10];
    // Tx and Rx areas are the same!!!
    uint8_t *dst = NULL;
    smm_t *smm = smm_init(tx, sizeof(tx)/sizeof(tx[0]),
			  tx, sizeof(tx)/sizeof(tx[0]));
    EXPECT_TRUE(NULL ==
		smm_copy_from_shmem(smm, dst, 0,
				    sizeof(tx)/sizeof(tx[0]) + 1));
}

TEST(TestSmmCopyFromShmem, AllocatedBufferDoesNotWrap) {
    uint8_t tx[10];
    //uint8_t rx[10];
    // Tx and Rx areas are the same!!!
    smm_t *smm = smm_init(tx, sizeof(tx)/sizeof(tx[0]),
			  tx, sizeof(tx)/sizeof(tx[0]));
    int32_t offset;
    char str[] = "Hello";
    size_t len = strlen(str) + 1;
    EXPECT_EQ(0, offset = smm_malloc(smm, len));
    EXPECT_EQ(offset, smm_copy_to_shmem(smm, offset, (uint8_t *)str, len));
    EXPECT_EQ((ssize_t)len, smm_buflen(smm, offset));
    uint8_t *dst = (uint8_t *)malloc(len);
    EXPECT_EQ(dst, smm_copy_from_shmem(smm, dst, offset, len));
    EXPECT_EQ(0, strcmp((char *)dst, str));
}

TEST(TestSmmCopyFromShmem, AllocatedBufferWraps) {
    uint8_t tx[10];
    //uint8_t rx[10];
    // Tx and Rx areas are the same!!!
    smm_t *smm = smm_init(tx, sizeof(tx)/sizeof(tx[0]),
			  tx, sizeof(tx)/sizeof(tx[0]));
    int32_t offset;
    char str[] = "Hello";
    size_t len = strlen(str) + 1;
    EXPECT_EQ(0, offset = smm_malloc(smm, len));
    EXPECT_EQ(offset, smm_copy_to_shmem(smm, offset, (uint8_t *)str, len));
    //EXPECT_EQ(1, smm_buflen(smm, offset + len));
    EXPECT_TRUE(smm_free(smm, offset));
    EXPECT_EQ((int32_t)len, offset = smm_malloc(smm, len));
    EXPECT_EQ(offset, smm_copy_to_shmem(smm, offset, (uint8_t *)str, len));
    EXPECT_EQ((ssize_t)len, smm_buflen(smm, offset));
    uint8_t *dst = (uint8_t *)malloc(len);
    EXPECT_EQ(dst, smm_copy_from_shmem(smm, dst, offset, len));
    EXPECT_EQ(0, strcmp((char *)dst, str));
}
