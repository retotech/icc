/*
 * ICC, Inter Cell Communication
 *
 * Copyright (c) Retotech AB, 2016-2017
 *
 * Authors:
 *  Jonas Weståker <jonas.westaker@retotech.se>
 *
 * This work is licensed under the terms of the MIT license.  See
 * the COPYING file in the top-level directory.
 */
#ifndef SMM_H
#define SMM_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>
#include <unistd.h>

struct smm;
typedef struct smm smm_t;

smm_t *
smm_init(uint8_t *tx_base, size_t tx_size, uint8_t *rx_base, size_t rx_size);

int32_t
smm_malloc(smm_t *smm, size_t len);

int32_t
smm_copy_to_shmem(smm_t *smm, int32_t dst_offset,
		  uint8_t *src, size_t len);

bool
smm_free(smm_t *smm, int32_t offset);

ssize_t
smm_buflen(smm_t *smm, int32_t offset);

uint8_t *
smm_copy_from_shmem(smm_t *smm, uint8_t *dst,
		    int32_t src_offset, size_t len);

#ifdef __cplusplus
}
#endif

#endif /* ICCT_IVSHMEM_DRV_H */
