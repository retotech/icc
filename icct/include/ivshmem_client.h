/*
 * ICC, Inter Cell Communication
 *
 * Copyright (c) Retotech AB, 2016-2017
 *
 * Authors:
 *  Jonas Weståker <jonas.westaker@retotech.se>
 *
 * This work is licensed under the terms of the MIT license.  See
 * the COPYING file in the top-level directory.
 */
#ifndef ICCT_IVSHMEM_CLIENT_H
#define ICCT_IVSHMEM_CLIENT_H

#include <stdint.h>
#include <stddef.h>

typedef struct ivshmem_client ivshmem_client_t;

struct ivshmem_client {
    uint8_t *(*malloc)(ivshmem_client_t *ivshmem_client, size_t len);
    void (*rx_msg)(ivshmem_client_t *ivshmem_client, uint8_t *data, size_t len);
};

#endif /* ICCT_IVSHMEM_CLIENT_H */
