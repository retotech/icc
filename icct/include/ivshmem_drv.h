/*
 * ICC, Inter Cell Communication
 *
 * Copyright (c) Retotech AB, 2016-2017
 *
 * Authors:
 *  Jonas Weståker <jonas.westaker@retotech.se>
 *
 * This work is licensed under the terms of the MIT license.  See
 * the COPYING file in the top-level directory.
 */
#ifndef ICCT_IVSHMEM_DRV_H
#define ICCT_IVSHMEM_DRV_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>

#include "ivshmem_client.h"
#include "smm.h"

typedef struct ivshmem_bar0 ivshmem_bar0_t;
typedef struct ivshmem_drv ivshmem_drv_t;

struct ivshmem_drv {
    ivshmem_client_t *client;
    ivshmem_bar0_t *bar0;
    smm_t *smm;
    bool (*tx_msg)(ivshmem_drv_t *ivshmem_drv, uint8_t *data, size_t len);
    bool (*handle_interrupt)(ivshmem_drv_t *ivshmem_drv);
};

ivshmem_drv_t *
ivshmem_drv_init(ivshmem_client_t *client,
		 uint8_t *bar0_base, size_t bar0_size,
		 uint8_t *tx_base, size_t tx_size,
		 uint8_t *rx_base, size_t rx_size);

#ifdef __cplusplus
}
#endif

#endif /* ICCT_IVSHMEM_DRV_H */
