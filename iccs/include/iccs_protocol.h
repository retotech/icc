/*
 * ICC, Inter Cell Communication
 *
 * Copyright (c) Retotech AB, 2016-2017
 *
 * Authors:
 *  Jonas Weståker <jonas.westaker@retotech.se>
 *
 * This work is licensed under the terms of the MIT license.  See
 * the COPYING file in the top-level directory.
 */
#ifndef ICCS_PROTOCOL_H
#define ICCS_PROTOCOL_H

#include <stdint.h>

#define ICCS_CONN_REQ 0
#define ICCS_CONN_RLY 1
#define ICCS_CONN_RST 2
#define ICCS_CONN_ALV 3
#define ICCS_UDATA_MSG 4

#define ICCS_CONN_REQ_NAME "ICCS_CONN_REQ"
#define ICCS_CONN_RLY_NAME "ICCS_CONN_RLY"
#define ICCS_CONN_RST_NAME "ICCS_CONN_RST"
#define ICCS_CONN_ALV_NAME "ICCS_CONN_ALV"
#define ICCS_UDATA_MSG_NAME "ICCS_UDATA_MSG"

static char *msgNames[] = {
    ICCS_CONN_REQ_NAME,
    ICCS_CONN_RLY_NAME,
    ICCS_CONN_RST_NAME,
    ICCS_CONN_ALV_NAME,
    ICCS_UDATA_MSG_NAME
};

static inline char *
iccsMsgName(uint8_t type) {
    return msgNames[type];
}

typedef struct {
    uint8_t type;
    uint8_t conn_id;    
} iccs_gen_hdr_t;

typedef struct {
    uint8_t type;
    uint8_t conn_id;    
} iccs_conn_req_t;

typedef struct {
    uint8_t type;
    uint8_t conn_id;    
} iccs_conn_rly_t;

typedef struct {
    uint8_t type;
    uint8_t conn_id;    
} iccs_conn_rst_t;

typedef struct {
    uint8_t type;
    uint8_t conn_id;    
} iccs_conn_alv_t;

typedef struct {
    uint8_t type;
    uint8_t conn_id;    
    uint16_t size;
    uint8_t udata[];
} iccs_udata_msg_t;

#endif /* #ifdef ICCS_PROTOCOL_H */
