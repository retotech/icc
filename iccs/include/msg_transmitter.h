/*
 * ICC, Inter Cell Communication
 *
 * Copyright (c) Retotech AB, 2016-2017
 *
 * Authors:
 *  Jonas Weståker <jonas.westaker@retotech.se>
 *
 * This work is licensed under the terms of the MIT license.  See
 * the COPYING file in the top-level directory.
 */
#ifndef MSG_TRANSMITTER_H
#define MSG_TRANSMITTER_H

#include "iccs_protocol.h"

struct msg_transmitter {
    void (*transmit_conn_req)(iccs_conn_req_t *msg);
    void (*transmit_conn_rly)(iccs_conn_rly_t *msg);
    void (*transmit_conn_alv)(iccs_conn_alv_t *msg);
    void (*transmit_conn_rst)(iccs_conn_rst_t *msg);
    void (*transmit_udata_msg)(iccs_udata_msg_t *msg);
};

typedef struct msg_transmitter msg_transmitter_t;

#endif /* MSG_TRANSMITTER_H */
