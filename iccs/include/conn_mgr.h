/*
 * ICC, Inter Cell Communication
 *
 * Copyright (c) Retotech AB, 2016-2017
 *
 * Authors:
 *  Jonas Weståker <jonas.westaker@retotech.se>
 *
 * This work is licensed under the terms of the MIT license.  See
 * the COPYING file in the top-level directory.
 */
#ifndef ICCS_CONN_MGR_H
#define ICCS_CONN_MGR_H

#include "conn_client.h"
#include "msg_transmitter.h"
#include "iccs_protocol.h"

struct iccs_conn;

typedef struct iccs_conn iccs_conn_t;

struct iccs_conn {
    int state;
    conn_client_t *client;
    msg_transmitter_t *transmitter;
    uint8_t id;
    void (*connect)(iccs_conn_t *conn);
    void (*receive_msg)(iccs_conn_t *conn, iccs_gen_hdr_t *msg);
    void (*transmit_udata)(iccs_conn_t *conn, uint8_t *buf, size_t len);
    void (*alive)(iccs_conn_t *conn);
    void (*disconnect)(iccs_conn_t *conn);
};

iccs_conn_t *
init(conn_client_t *client, msg_transmitter_t *transmitter);

#endif /* #ifndef ICCS_CONN_MGR_H */
