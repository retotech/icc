/*
 * ICC, Inter Cell Communication
 *
 * Copyright (c) Retotech AB, 2016-2017
 *
 * Authors:
 *  Jonas Weståker <jonas.westaker@retotech.se>
 *
 * This work is licensed under the terms of the MIT license.  See
 * the COPYING file in the top-level directory.
 */
#ifndef ICCS_CONN_CLIENT_H
#define ICCS_CONN_CLIENT_H

struct conn_client {
    void (*connected)(void);
    void * (*msg_alloc)(size_t len);
    void (*msg_deliver)(void *msg, size_t len);
    void (*msg_free)(void *msg);
    void (*disconnected)(void);
};

typedef struct conn_client conn_client_t;

#endif /* #define ICCS_CONN_CLIENT_H */
