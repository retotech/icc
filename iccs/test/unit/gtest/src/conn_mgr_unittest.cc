#include <gtest/gtest.h>

#include "conn_mgr.h"
#include "conn_client.h"
#include "msg_transmitter.h"

static bool client_connected;
static bool client_msg_alloc;
static bool client_msg_deliver;
static bool client_msg_free;
static bool client_disconnected;

static void
connected(void) {
    client_connected = true;
}

static void *
msg_alloc(size_t len) {
    client_msg_alloc = true;
    return malloc(len);
}

static void
msg_deliver(void *msg, size_t len) {
    (void)msg;
    (void)len;
    client_msg_deliver = true;
}

static void
msg_free(void *msg) {
    (void)msg;
    client_msg_free = true;
}

static void
disconnected(void) {
    client_disconnected = true;
}

static conn_client_t testClient = {
    .connected = connected,
    .msg_alloc = msg_alloc,
    .msg_deliver = msg_deliver,
    .msg_free = msg_free,
    .disconnected = disconnected,
};

static bool conn_req_transmitted;
static bool conn_rly_transmitted;
static bool conn_rst_transmitted;
static bool conn_alv_transmitted;
static bool udata_msg_transmitted;

static void
transmit_conn_req(iccs_conn_req_t *msg) {
    (void)msg;
    //printf("%s\n", __func__);
    conn_req_transmitted = true;
}

static void
transmit_conn_rly(iccs_conn_rly_t *msg) {
    (void)msg;
    //printf("%s\n", __func__);
    conn_rly_transmitted = true;
}

static void
transmit_conn_rst(iccs_conn_rst_t *msg) {
    (void)msg;
    //printf("%s\n", __func__);
    conn_rst_transmitted = true;
}

static void
transmit_conn_alv(iccs_conn_alv_t *msg) {
    (void)msg;
    //printf("%s\n", __func__);
    conn_alv_transmitted = true;
}

static void
transmit_udata_msg(iccs_udata_msg_t *msg) {
    (void)msg;
    //printf("%s\n", __func__);
    udata_msg_transmitted = true;
}

static msg_transmitter_t testTransmitter = {
    transmit_conn_req,
    transmit_conn_rly,
    transmit_conn_alv,
    transmit_conn_rst,
    transmit_udata_msg,
};

msg_transmitter_t *
initTransmitter() {
    conn_req_transmitted = false;
    conn_rly_transmitted = false;
    conn_rst_transmitted = false;
    conn_alv_transmitted = false;
    udata_msg_transmitted = false;
    return &testTransmitter;
}

// TestConnMgrInDisconnectedState

TEST(TestConnMgrInDisconnectedState, Connect) {
    msg_transmitter_t *transmitter = initTransmitter();
    iccs_conn_t *conn = init(&testClient, transmitter);
    EXPECT_FALSE(NULL == conn);

    conn->connect(conn);
    EXPECT_TRUE(conn_req_transmitted);
    EXPECT_FALSE(conn_rly_transmitted);
    EXPECT_FALSE(conn_rst_transmitted);
    EXPECT_FALSE(conn_alv_transmitted);
    EXPECT_FALSE(udata_msg_transmitted);
}

TEST(TestConnMgrInDisconnectedState, Transmit) {
    msg_transmitter_t *transmitter = initTransmitter();
    iccs_conn_t *conn = init(&testClient, transmitter);
    EXPECT_FALSE(NULL == conn);

    uint8_t buf[] = {0x00, 0x01, 0x02, 0x03, 0x04};
    conn->transmit_udata(conn, buf, sizeof(buf)/sizeof(buf[0]));
    EXPECT_FALSE(conn_req_transmitted);
    EXPECT_FALSE(conn_rly_transmitted);
    EXPECT_FALSE(conn_rst_transmitted);
    EXPECT_FALSE(conn_alv_transmitted);
    EXPECT_FALSE(udata_msg_transmitted);
}

TEST(TestConnMgrInDisconnectedState, Alive) {
    msg_transmitter_t *transmitter = initTransmitter();
    iccs_conn_t *conn = init(&testClient, transmitter);
    EXPECT_FALSE(NULL == conn);

    conn->alive(conn);
    EXPECT_FALSE(conn_req_transmitted);
    EXPECT_FALSE(conn_rly_transmitted);
    EXPECT_FALSE(conn_rst_transmitted);
    EXPECT_FALSE(conn_alv_transmitted);
    EXPECT_FALSE(udata_msg_transmitted);
}

TEST(TestConnMgrInDisconnectedState, Disconnect) {
    msg_transmitter_t *transmitter = initTransmitter();
    iccs_conn_t *conn = init(&testClient, transmitter);
    EXPECT_FALSE(NULL == conn);

    conn->disconnect(conn);
    EXPECT_FALSE(conn_req_transmitted);
    EXPECT_FALSE(conn_rly_transmitted);
    EXPECT_FALSE(conn_rst_transmitted);
    EXPECT_FALSE(conn_alv_transmitted);
    EXPECT_FALSE(udata_msg_transmitted);
}

TEST(TestConnMgrInDisconnectedState, HandleConnReqMsg) {
    msg_transmitter_t *transmitter = initTransmitter();
    iccs_conn_t *conn = init(&testClient, transmitter);
    EXPECT_FALSE(NULL == conn);

    iccs_conn_req_t msg = {
	.type = ICCS_CONN_REQ,
	.conn_id = 0,
    };
    conn->receive_msg(conn, (iccs_gen_hdr_t *)&msg);
    EXPECT_FALSE(conn_req_transmitted);
    EXPECT_TRUE(conn_rly_transmitted);
    EXPECT_FALSE(conn_rst_transmitted);
    EXPECT_FALSE(conn_alv_transmitted);
    EXPECT_FALSE(udata_msg_transmitted);
}

TEST(TestConnMgrInDisconnectedState, HandleConnRlyMsg) {
    msg_transmitter_t *transmitter = initTransmitter();
    iccs_conn_t *conn = init(&testClient, transmitter);
    EXPECT_FALSE(NULL == conn);

    iccs_conn_rly_t msg = {
	.type = ICCS_CONN_RLY,
	.conn_id = 0,
    };
    conn->receive_msg(conn, (iccs_gen_hdr_t *)&msg);
    EXPECT_FALSE(conn_req_transmitted);
    EXPECT_FALSE(conn_rly_transmitted);
    EXPECT_FALSE(conn_rst_transmitted);
    EXPECT_FALSE(conn_alv_transmitted);
    EXPECT_FALSE(udata_msg_transmitted);
}

TEST(TestConnMgrInDisconnectedState, HandleConnRstMsg) {
    msg_transmitter_t *transmitter = initTransmitter();
    iccs_conn_t *conn = init(&testClient, transmitter);
    EXPECT_FALSE(NULL == conn);

    iccs_conn_rst_t msg = {
	.type = ICCS_CONN_RST,
	.conn_id = 0,
    };
    conn->receive_msg(conn, (iccs_gen_hdr_t *)&msg);
    EXPECT_FALSE(conn_req_transmitted);
    EXPECT_FALSE(conn_rly_transmitted);
    EXPECT_FALSE(conn_rst_transmitted);
    EXPECT_FALSE(conn_alv_transmitted);
    EXPECT_FALSE(udata_msg_transmitted);
}

TEST(TestConnMgrInDisconnectedState, HandleConnAlvMsg) {
    msg_transmitter_t *transmitter = initTransmitter();
    iccs_conn_t *conn = init(&testClient, transmitter);
    EXPECT_FALSE(NULL == conn);

    iccs_conn_alv_t msg = {
	.type = ICCS_CONN_ALV,
	.conn_id = 0,
    };
    conn->receive_msg(conn, (iccs_gen_hdr_t *)&msg);
    EXPECT_FALSE(conn_req_transmitted);
    EXPECT_FALSE(conn_rly_transmitted);
    EXPECT_FALSE(conn_rst_transmitted);
    EXPECT_FALSE(conn_alv_transmitted);
    EXPECT_FALSE(udata_msg_transmitted);
}

// TestConnMgrInConnectingMasterState

TEST(TestConnMgrInConnectingMasterState, Connect) {
    msg_transmitter_t *transmitter = initTransmitter();
    iccs_conn_t *conn = init(&testClient, transmitter);
    EXPECT_FALSE(NULL == conn);

    conn->connect(conn);
    transmitter = initTransmitter();

    conn->connect(conn);
    EXPECT_FALSE(conn_req_transmitted);
    EXPECT_FALSE(conn_rly_transmitted);
    EXPECT_FALSE(conn_rst_transmitted);
    EXPECT_FALSE(conn_alv_transmitted);
    EXPECT_FALSE(udata_msg_transmitted);
}

TEST(TestConnMgrInConnectingMasterState, Transmit) {
    msg_transmitter_t *transmitter = initTransmitter();
    iccs_conn_t *conn = init(&testClient, transmitter);
    EXPECT_FALSE(NULL == conn);

    conn->connect(conn);
    transmitter = initTransmitter();

    uint8_t buf[] = {0x00, 0x01, 0x02, 0x03, 0x04};
    conn->transmit_udata(conn, buf, sizeof(buf)/sizeof(buf[0]));
    EXPECT_FALSE(conn_req_transmitted);
    EXPECT_FALSE(conn_rly_transmitted);
    EXPECT_FALSE(conn_rst_transmitted);
    EXPECT_FALSE(conn_alv_transmitted);
    EXPECT_FALSE(udata_msg_transmitted);
}

TEST(TestConnMgrInConnectingMasterState, Alive) {
    msg_transmitter_t *transmitter = initTransmitter();
    iccs_conn_t *conn = init(&testClient, transmitter);
    EXPECT_FALSE(NULL == conn);

    conn->connect(conn);
    transmitter = initTransmitter();

    conn->alive(conn);
    EXPECT_FALSE(conn_req_transmitted);
    EXPECT_FALSE(conn_rly_transmitted);
    EXPECT_FALSE(conn_rst_transmitted);
    EXPECT_FALSE(conn_alv_transmitted);
    EXPECT_FALSE(udata_msg_transmitted);
}

TEST(TestConnMgrInConnectingMasterState, Disconnect) {
    msg_transmitter_t *transmitter = initTransmitter();
    iccs_conn_t *conn = init(&testClient, transmitter);
    EXPECT_FALSE(NULL == conn);

    conn->connect(conn);
    transmitter = initTransmitter();

    conn->disconnect(conn);
    EXPECT_FALSE(conn_req_transmitted);
    EXPECT_FALSE(conn_rly_transmitted);
    EXPECT_FALSE(conn_rst_transmitted);
    EXPECT_FALSE(conn_alv_transmitted);
    EXPECT_FALSE(udata_msg_transmitted);
}

TEST(TestConnMgrInConnectingMasterState, HandleConnReqMsg) {
    msg_transmitter_t *transmitter = initTransmitter();
    iccs_conn_t *conn = init(&testClient, transmitter);
    EXPECT_FALSE(NULL == conn);

    conn->connect(conn);
    transmitter = initTransmitter();

    iccs_conn_req_t msg = {
	.type = ICCS_CONN_REQ,
	.conn_id = 0,
    };
    conn->receive_msg(conn, (iccs_gen_hdr_t *)&msg);
    EXPECT_FALSE(conn_req_transmitted);
    EXPECT_FALSE(conn_rly_transmitted);
    EXPECT_FALSE(conn_rst_transmitted);
    EXPECT_FALSE(conn_alv_transmitted);
    EXPECT_FALSE(udata_msg_transmitted);
}

TEST(TestConnMgrInConnectingMasterState, HandleConnRlyMsg) {
    msg_transmitter_t *transmitter = initTransmitter();
    iccs_conn_t *conn = init(&testClient, transmitter);
    EXPECT_FALSE(NULL == conn);

    conn->connect(conn);
    transmitter = initTransmitter();

    iccs_conn_rly_t msg = {
	.type = ICCS_CONN_RLY,
	.conn_id = 0,
    };
    conn->receive_msg(conn, (iccs_gen_hdr_t *)&msg);
    EXPECT_FALSE(conn_req_transmitted);
    EXPECT_FALSE(conn_rly_transmitted);
    EXPECT_FALSE(conn_rst_transmitted);
    EXPECT_FALSE(conn_alv_transmitted);
    EXPECT_FALSE(udata_msg_transmitted);
}

TEST(TestConnMgrInConnectingMasterState, HandleConnRstMsg) {
    msg_transmitter_t *transmitter = initTransmitter();
    iccs_conn_t *conn = init(&testClient, transmitter);
    EXPECT_FALSE(NULL == conn);

    conn->connect(conn);
    transmitter = initTransmitter();

    iccs_conn_rst_t msg = {
	.type = ICCS_CONN_RST,
	.conn_id = 0,
    };
    conn->receive_msg(conn, (iccs_gen_hdr_t *)&msg);
    EXPECT_FALSE(conn_req_transmitted);
    EXPECT_FALSE(conn_rly_transmitted);
    EXPECT_FALSE(conn_rst_transmitted);
    EXPECT_FALSE(conn_alv_transmitted);
    EXPECT_FALSE(udata_msg_transmitted);
}

TEST(TestConnMgrInConnectingMasterState, HandleConnAlvMsg) {
    msg_transmitter_t *transmitter = initTransmitter();
    iccs_conn_t *conn = init(&testClient, transmitter);
    EXPECT_FALSE(NULL == conn);

    conn->connect(conn);
    transmitter = initTransmitter();

    iccs_conn_alv_t msg = {
	.type = ICCS_CONN_ALV,
	.conn_id = 0,
    };
    conn->receive_msg(conn, (iccs_gen_hdr_t *)&msg);
    EXPECT_FALSE(conn_req_transmitted);
    EXPECT_FALSE(conn_rly_transmitted);
    EXPECT_FALSE(conn_rst_transmitted);
    EXPECT_FALSE(conn_alv_transmitted);
    EXPECT_FALSE(udata_msg_transmitted);
}

// TestConnMgrInConnectingSlaveState

TEST(TestConnMgrInConnectingSlaveState, Connect) {
    msg_transmitter_t *transmitter = initTransmitter();
    iccs_conn_t *conn = init(&testClient, transmitter);
    EXPECT_FALSE(NULL == conn);

    iccs_conn_req_t msg = {
	.type = ICCS_CONN_REQ,
	.conn_id = 0,
    };
    conn->receive_msg(conn, (iccs_gen_hdr_t *)&msg);
    transmitter = initTransmitter();

    conn->connect(conn);
    EXPECT_FALSE(conn_req_transmitted);
    EXPECT_FALSE(conn_rly_transmitted);
    EXPECT_FALSE(conn_rst_transmitted);
    EXPECT_FALSE(conn_alv_transmitted);
    EXPECT_FALSE(udata_msg_transmitted);
}

TEST(TestConnMgrInConnectingSlaveState, Transmit) {
    msg_transmitter_t *transmitter = initTransmitter();
    iccs_conn_t *conn = init(&testClient, transmitter);
    EXPECT_FALSE(NULL == conn);

    iccs_conn_req_t msg = {
	.type = ICCS_CONN_REQ,
	.conn_id = 0,
    };
    conn->receive_msg(conn, (iccs_gen_hdr_t *)&msg);
    transmitter = initTransmitter();

    uint8_t buf[] = {0x00, 0x01, 0x02, 0x03, 0x04};
    conn->transmit_udata(conn, buf, sizeof(buf)/sizeof(buf[0]));
    EXPECT_FALSE(conn_req_transmitted);
    EXPECT_FALSE(conn_rly_transmitted);
    EXPECT_FALSE(conn_rst_transmitted);
    EXPECT_FALSE(conn_alv_transmitted);
    EXPECT_FALSE(udata_msg_transmitted);
}

TEST(TestConnMgrInConnectingSlaveState, Alive) {
    msg_transmitter_t *transmitter = initTransmitter();
    iccs_conn_t *conn = init(&testClient, transmitter);
    EXPECT_FALSE(NULL == conn);

    iccs_conn_req_t msg = {
	.type = ICCS_CONN_REQ,
	.conn_id = 0,
    };
    conn->receive_msg(conn, (iccs_gen_hdr_t *)&msg);
    transmitter = initTransmitter();

    conn->alive(conn);
    EXPECT_FALSE(conn_req_transmitted);
    EXPECT_FALSE(conn_rly_transmitted);
    EXPECT_FALSE(conn_rst_transmitted);
    EXPECT_FALSE(conn_alv_transmitted);
    EXPECT_FALSE(udata_msg_transmitted);
}

TEST(TestConnMgrInConnectingSlaveState, Disconnect) {
    msg_transmitter_t *transmitter = initTransmitter();
    iccs_conn_t *conn = init(&testClient, transmitter);
    EXPECT_FALSE(NULL == conn);

    iccs_conn_req_t msg = {
	.type = ICCS_CONN_REQ,
	.conn_id = 0,
    };
    conn->receive_msg(conn, (iccs_gen_hdr_t *)&msg);
    transmitter = initTransmitter();

    conn->connect(conn);
    EXPECT_FALSE(conn_req_transmitted);
    EXPECT_FALSE(conn_rly_transmitted);
    EXPECT_FALSE(conn_rst_transmitted);
    EXPECT_FALSE(conn_alv_transmitted);
    EXPECT_FALSE(udata_msg_transmitted);
}

TEST(TestConnMgrInConnectingSlaveState, HandleConnReqMsg) {
    msg_transmitter_t *transmitter = initTransmitter();
    iccs_conn_t *conn = init(&testClient, transmitter);
    EXPECT_FALSE(NULL == conn);

    iccs_conn_req_t conn_req_msg = {
	.type = ICCS_CONN_REQ,
	.conn_id = 0,
    };
    conn->receive_msg(conn, (iccs_gen_hdr_t *)&conn_req_msg);
    transmitter = initTransmitter();

    iccs_conn_req_t msg = {
	.type = ICCS_CONN_REQ,
	.conn_id = 0,
    };
    conn->receive_msg(conn, (iccs_gen_hdr_t *)&msg);
    EXPECT_FALSE(conn_req_transmitted);
    EXPECT_FALSE(conn_rly_transmitted);
    EXPECT_FALSE(conn_rst_transmitted);
    EXPECT_FALSE(conn_alv_transmitted);
    EXPECT_FALSE(udata_msg_transmitted);
}

TEST(TestConnMgrInConnectingSlaveState, HandleConnRlyMsg) {
    msg_transmitter_t *transmitter = initTransmitter();
    iccs_conn_t *conn = init(&testClient, transmitter);
    EXPECT_FALSE(NULL == conn);

    iccs_conn_req_t conn_req_msg = {
	.type = ICCS_CONN_REQ,
	.conn_id = 0,
    };
    conn->receive_msg(conn, (iccs_gen_hdr_t *)&conn_req_msg);
    transmitter = initTransmitter();

    iccs_conn_rly_t msg = {
	.type = ICCS_CONN_RLY,
	.conn_id = 0,
    };
    conn->receive_msg(conn, (iccs_gen_hdr_t *)&msg);
    EXPECT_FALSE(conn_req_transmitted);
    EXPECT_FALSE(conn_rly_transmitted);
    EXPECT_FALSE(conn_rst_transmitted);
    EXPECT_FALSE(conn_alv_transmitted);
    EXPECT_FALSE(udata_msg_transmitted);
}

TEST(TestConnMgrInConnectingSlaveState, HandleConnRstMsg) {
    msg_transmitter_t *transmitter = initTransmitter();
    iccs_conn_t *conn = init(&testClient, transmitter);
    EXPECT_FALSE(NULL == conn);

    iccs_conn_req_t conn_req_msg = {
	.type = ICCS_CONN_REQ,
	.conn_id = 0,
    };
    conn->receive_msg(conn, (iccs_gen_hdr_t *)&conn_req_msg);
    transmitter = initTransmitter();

    iccs_conn_rst_t msg = {
	.type = ICCS_CONN_RST,
	.conn_id = 0,
    };
    conn->receive_msg(conn, (iccs_gen_hdr_t *)&msg);
    EXPECT_FALSE(conn_req_transmitted);
    EXPECT_FALSE(conn_rly_transmitted);
    EXPECT_FALSE(conn_rst_transmitted);
    EXPECT_FALSE(conn_alv_transmitted);
    EXPECT_FALSE(udata_msg_transmitted);
}

TEST(TestConnMgrInConnectingSlaveState, HandleConnAlvMsg) {
    msg_transmitter_t *transmitter = initTransmitter();
    iccs_conn_t *conn = init(&testClient, transmitter);
    EXPECT_FALSE(NULL == conn);

    iccs_conn_req_t conn_req_msg = {
	.type = ICCS_CONN_REQ,
	.conn_id = 0,
    };
    conn->receive_msg(conn, (iccs_gen_hdr_t *)&conn_req_msg);
    transmitter = initTransmitter();

    iccs_conn_alv_t msg = {
	.type = ICCS_CONN_ALV,
	.conn_id = 0,
    };
    conn->receive_msg(conn, (iccs_gen_hdr_t *)&msg);
    EXPECT_FALSE(conn_req_transmitted);
    EXPECT_FALSE(conn_rly_transmitted);
    EXPECT_FALSE(conn_rst_transmitted);
    EXPECT_FALSE(conn_alv_transmitted);
    EXPECT_FALSE(udata_msg_transmitted);
}

// TestConnMgrInConnectedState

TEST(TestConnMgrInConnectedState, Connect) {
    msg_transmitter_t *transmitter = initTransmitter();
    iccs_conn_t *conn = init(&testClient, transmitter);
    EXPECT_FALSE(NULL == conn);

    conn->connect(conn);
    iccs_conn_rly_t msg = {
	.type = ICCS_CONN_RLY,
	.conn_id = 0,
    };
    conn->receive_msg(conn, (iccs_gen_hdr_t *)&msg);
    transmitter = initTransmitter();

    conn->connect(conn);
    EXPECT_FALSE(conn_req_transmitted);
    EXPECT_FALSE(conn_rly_transmitted);
    EXPECT_FALSE(conn_rst_transmitted);
    EXPECT_FALSE(conn_alv_transmitted);
    EXPECT_FALSE(udata_msg_transmitted);
}

TEST(TestConnMgrInConnectedState, Transmit) {
    msg_transmitter_t *transmitter = initTransmitter();
    iccs_conn_t *conn = init(&testClient, transmitter);
    EXPECT_FALSE(NULL == conn);

    conn->connect(conn);
    iccs_conn_rly_t msg = {
	.type = ICCS_CONN_RLY,
	.conn_id = 0,
    };
    conn->receive_msg(conn, (iccs_gen_hdr_t *)&msg);
    transmitter = initTransmitter();

    uint8_t buf[] = {0x00, 0x01, 0x02, 0x03, 0x04};
    conn->transmit_udata(conn, buf, sizeof(buf)/sizeof(buf[0]));
    EXPECT_FALSE(conn_req_transmitted);
    EXPECT_FALSE(conn_rly_transmitted);
    EXPECT_FALSE(conn_rst_transmitted);
    EXPECT_FALSE(conn_alv_transmitted);
    EXPECT_TRUE(udata_msg_transmitted);
}

TEST(TestConnMgrInConnectedState, Alive) {
    msg_transmitter_t *transmitter = initTransmitter();
    iccs_conn_t *conn = init(&testClient, transmitter);
    EXPECT_FALSE(NULL == conn);

    conn->connect(conn);
    iccs_conn_rly_t msg = {
	.type = ICCS_CONN_RLY,
	.conn_id = 0,
    };
    conn->receive_msg(conn, (iccs_gen_hdr_t *)&msg);
    transmitter = initTransmitter();

    conn->alive(conn);
    EXPECT_FALSE(conn_req_transmitted);
    EXPECT_FALSE(conn_rly_transmitted);
    EXPECT_FALSE(conn_rst_transmitted);
    EXPECT_TRUE(conn_alv_transmitted);
    EXPECT_FALSE(udata_msg_transmitted);
}

TEST(TestConnMgrInConnectedState, Disconnect) {
    msg_transmitter_t *transmitter = initTransmitter();
    iccs_conn_t *conn = init(&testClient, transmitter);
    EXPECT_FALSE(NULL == conn);

    conn->connect(conn);
    iccs_conn_rly_t msg = {
	.type = ICCS_CONN_RLY,
	.conn_id = 0,
    };
    conn->receive_msg(conn, (iccs_gen_hdr_t *)&msg);
    transmitter = initTransmitter();

    conn->disconnect(conn);
    EXPECT_FALSE(conn_req_transmitted);
    EXPECT_FALSE(conn_rly_transmitted);
    EXPECT_TRUE(conn_rst_transmitted);
    EXPECT_FALSE(conn_alv_transmitted);
    EXPECT_FALSE(udata_msg_transmitted);
}

TEST(TestConnMgrInConnectedState, HandleConnReqMsg) {
    msg_transmitter_t *transmitter = initTransmitter();
    iccs_conn_t *conn = init(&testClient, transmitter);
    EXPECT_FALSE(NULL == conn);

    conn->connect(conn);
    iccs_conn_rly_t msg = {
	.type = ICCS_CONN_RLY,
	.conn_id = 0,
    };
    conn->receive_msg(conn, (iccs_gen_hdr_t *)&msg);
    transmitter = initTransmitter();

    iccs_conn_req_t conn_req_msg = {
	.type = ICCS_CONN_REQ,
	.conn_id = 0,
    };
    conn->receive_msg(conn, (iccs_gen_hdr_t *)&conn_req_msg);
    EXPECT_FALSE(conn_req_transmitted);
    EXPECT_FALSE(conn_rly_transmitted);
    EXPECT_FALSE(conn_rst_transmitted);
    EXPECT_FALSE(conn_alv_transmitted);
    EXPECT_FALSE(udata_msg_transmitted);
}

TEST(TestConnMgrInConnectedState, HandleConnRlyMsg) {
    msg_transmitter_t *transmitter = initTransmitter();
    iccs_conn_t *conn = init(&testClient, transmitter);
    EXPECT_FALSE(NULL == conn);

    conn->connect(conn);
    iccs_conn_rly_t msg = {
	.type = ICCS_CONN_RLY,
	.conn_id = 0,
    };
    conn->receive_msg(conn, (iccs_gen_hdr_t *)&msg);
    transmitter = initTransmitter();

    iccs_conn_rly_t conn_rly_msg = {
	.type = ICCS_CONN_RLY,
	.conn_id = 0,
    };
    conn->receive_msg(conn, (iccs_gen_hdr_t *)&conn_rly_msg);
    EXPECT_FALSE(conn_req_transmitted);
    EXPECT_FALSE(conn_rly_transmitted);
    EXPECT_FALSE(conn_rst_transmitted);
    EXPECT_FALSE(conn_alv_transmitted);
    EXPECT_FALSE(udata_msg_transmitted);
}

TEST(TestConnMgrInConnectedState, HandleConnRstMsg) {
    msg_transmitter_t *transmitter = initTransmitter();
    iccs_conn_t *conn = init(&testClient, transmitter);
    EXPECT_FALSE(NULL == conn);

    conn->connect(conn);
    iccs_conn_rly_t msg = {
	.type = ICCS_CONN_RLY,
	.conn_id = 0,
    };
    conn->receive_msg(conn, (iccs_gen_hdr_t *)&msg);
    transmitter = initTransmitter();

    iccs_conn_rst_t conn_rst_msg = {
	.type = ICCS_CONN_RST,
	.conn_id = 0,
    };
    conn->receive_msg(conn, (iccs_gen_hdr_t *)&conn_rst_msg);
    EXPECT_FALSE(conn_req_transmitted);
    EXPECT_FALSE(conn_rly_transmitted);
    EXPECT_TRUE(conn_rst_transmitted);
    EXPECT_FALSE(conn_alv_transmitted);
    EXPECT_FALSE(udata_msg_transmitted);
}

TEST(TestConnMgrInConnectedState, HandleConnAlvMsg) {
    msg_transmitter_t *transmitter = initTransmitter();
    iccs_conn_t *conn = init(&testClient, transmitter);
    EXPECT_FALSE(NULL == conn);

    conn->connect(conn);
    iccs_conn_rly_t msg = {
	.type = ICCS_CONN_RLY,
	.conn_id = 0,
    };
    conn->receive_msg(conn, (iccs_gen_hdr_t *)&msg);
    transmitter = initTransmitter();

    iccs_conn_alv_t conn_alv_msg = {
	.type = ICCS_CONN_ALV,
	.conn_id = 0,
    };
    conn->receive_msg(conn, (iccs_gen_hdr_t *)&conn_alv_msg);
    EXPECT_FALSE(conn_req_transmitted);
    EXPECT_FALSE(conn_rly_transmitted);
    EXPECT_FALSE(conn_rst_transmitted);
    EXPECT_FALSE(conn_alv_transmitted);
    EXPECT_FALSE(udata_msg_transmitted);
}

// TestConnMgrInDisconnectingState

TEST(TestConnMgrInDisconnectingState, Connect) {
    msg_transmitter_t *transmitter = initTransmitter();
    iccs_conn_t *conn = init(&testClient, transmitter);
    EXPECT_FALSE(NULL == conn);

    conn->connect(conn);
    iccs_conn_rly_t msg = {
	.type = ICCS_CONN_RLY,
	.conn_id = 0,
    };
    conn->receive_msg(conn, (iccs_gen_hdr_t *)&msg);
    conn->disconnect(conn);
    transmitter = initTransmitter();

    conn->connect(conn);
    EXPECT_FALSE(conn_req_transmitted);
    EXPECT_FALSE(conn_rly_transmitted);
    EXPECT_FALSE(conn_rst_transmitted);
    EXPECT_FALSE(conn_alv_transmitted);
    EXPECT_FALSE(udata_msg_transmitted);
}

TEST(TestConnMgrInDisconnectingState, Transmit) {
    msg_transmitter_t *transmitter = initTransmitter();
    iccs_conn_t *conn = init(&testClient, transmitter);
    EXPECT_FALSE(NULL == conn);

    conn->connect(conn);
    iccs_conn_rly_t msg = {
	.type = ICCS_CONN_RLY,
	.conn_id = 0,
    };
    conn->receive_msg(conn, (iccs_gen_hdr_t *)&msg);
    conn->disconnect(conn);
    transmitter = initTransmitter();

    uint8_t buf[] = {0x00, 0x01, 0x02, 0x03, 0x04};
    conn->transmit_udata(conn, buf, sizeof(buf)/sizeof(buf[0]));
    EXPECT_FALSE(conn_req_transmitted);
    EXPECT_FALSE(conn_rly_transmitted);
    EXPECT_FALSE(conn_rst_transmitted);
    EXPECT_FALSE(conn_alv_transmitted);
    EXPECT_FALSE(udata_msg_transmitted);
}

TEST(TestConnMgrInDisconnectingState, Alive) {
    msg_transmitter_t *transmitter = initTransmitter();
    iccs_conn_t *conn = init(&testClient, transmitter);
    EXPECT_FALSE(NULL == conn);

    conn->connect(conn);
    iccs_conn_rly_t msg = {
	.type = ICCS_CONN_RLY,
	.conn_id = 0,
    };
    conn->receive_msg(conn, (iccs_gen_hdr_t *)&msg);
    conn->disconnect(conn);
    transmitter = initTransmitter();

    conn->alive(conn);
    EXPECT_FALSE(conn_req_transmitted);
    EXPECT_FALSE(conn_rly_transmitted);
    EXPECT_FALSE(conn_rst_transmitted);
    EXPECT_FALSE(conn_alv_transmitted);
    EXPECT_FALSE(udata_msg_transmitted);
}

TEST(TestConnMgrInDisconnectingState, Disconnect) {
    msg_transmitter_t *transmitter = initTransmitter();
    iccs_conn_t *conn = init(&testClient, transmitter);
    EXPECT_FALSE(NULL == conn);

    conn->connect(conn);
    iccs_conn_rly_t msg = {
	.type = ICCS_CONN_RLY,
	.conn_id = 0,
    };
    conn->receive_msg(conn, (iccs_gen_hdr_t *)&msg);
    conn->disconnect(conn);
    transmitter = initTransmitter();

    conn->disconnect(conn);
    EXPECT_FALSE(conn_req_transmitted);
    EXPECT_FALSE(conn_rly_transmitted);
    EXPECT_FALSE(conn_rst_transmitted);
    EXPECT_FALSE(conn_alv_transmitted);
    EXPECT_FALSE(udata_msg_transmitted);
}

TEST(TestConnMgrInDisconnectingState, HandleConnReqMsg) {
    msg_transmitter_t *transmitter = initTransmitter();
    iccs_conn_t *conn = init(&testClient, transmitter);
    EXPECT_FALSE(NULL == conn);

    conn->connect(conn);
    iccs_conn_rly_t msg = {
	.type = ICCS_CONN_RLY,
	.conn_id = 0,
    };
    conn->receive_msg(conn, (iccs_gen_hdr_t *)&msg);
    conn->disconnect(conn);
    transmitter = initTransmitter();

    iccs_conn_req_t conn_req_msg = {
	.type = ICCS_CONN_REQ,
	.conn_id = 0,
    };
    conn->receive_msg(conn, (iccs_gen_hdr_t *)&conn_req_msg);
    EXPECT_FALSE(conn_req_transmitted);
    EXPECT_FALSE(conn_rly_transmitted);
    EXPECT_FALSE(conn_rst_transmitted);
    EXPECT_FALSE(conn_alv_transmitted);
    EXPECT_FALSE(udata_msg_transmitted);
}

TEST(TestConnMgrInDisconnectingState, HandleConnRlyMsg) {
    msg_transmitter_t *transmitter = initTransmitter();
    iccs_conn_t *conn = init(&testClient, transmitter);
    EXPECT_FALSE(NULL == conn);

    conn->connect(conn);
    iccs_conn_rly_t msg = {
	.type = ICCS_CONN_RLY,
	.conn_id = 0,
    };
    conn->receive_msg(conn, (iccs_gen_hdr_t *)&msg);
    conn->disconnect(conn);
    transmitter = initTransmitter();

    iccs_conn_rly_t conn_rly_msg = {
	.type = ICCS_CONN_RLY,
	.conn_id = 0,
    };
    conn->receive_msg(conn, (iccs_gen_hdr_t *)&conn_rly_msg);
    EXPECT_FALSE(conn_req_transmitted);
    EXPECT_FALSE(conn_rly_transmitted);
    EXPECT_FALSE(conn_rst_transmitted);
    EXPECT_FALSE(conn_alv_transmitted);
    EXPECT_FALSE(udata_msg_transmitted);
}

TEST(TestConnMgrInDisconnectingState, HandleConnRstMsg) {
    msg_transmitter_t *transmitter = initTransmitter();
    iccs_conn_t *conn = init(&testClient, transmitter);
    EXPECT_FALSE(NULL == conn);

    conn->connect(conn);
    iccs_conn_rly_t msg = {
	.type = ICCS_CONN_RLY,
	.conn_id = 0,
    };
    conn->receive_msg(conn, (iccs_gen_hdr_t *)&msg);
    conn->disconnect(conn);
    transmitter = initTransmitter();

    iccs_conn_rst_t conn_rst_msg = {
	.type = ICCS_CONN_RST,
	.conn_id = 0,
    };
    conn->receive_msg(conn, (iccs_gen_hdr_t *)&conn_rst_msg);
    EXPECT_FALSE(conn_req_transmitted);
    EXPECT_FALSE(conn_rly_transmitted);
    EXPECT_FALSE(conn_rst_transmitted);
    EXPECT_FALSE(conn_alv_transmitted);
    EXPECT_FALSE(udata_msg_transmitted);
}

TEST(TestConnMgrInDisconnectingState, HandleConnAlvMsg) {
    msg_transmitter_t *transmitter = initTransmitter();
    iccs_conn_t *conn = init(&testClient, transmitter);
    EXPECT_FALSE(NULL == conn);

    conn->connect(conn);
    iccs_conn_rly_t msg = {
	.type = ICCS_CONN_RLY,
	.conn_id = 0,
    };
    conn->receive_msg(conn, (iccs_gen_hdr_t *)&msg);
    conn->disconnect(conn);
    transmitter = initTransmitter();

    iccs_conn_alv_t conn_alv_msg = {
	.type = ICCS_CONN_ALV,
	.conn_id = 0,
    };
    conn->receive_msg(conn, (iccs_gen_hdr_t *)&conn_alv_msg);
    EXPECT_FALSE(conn_req_transmitted);
    EXPECT_FALSE(conn_rly_transmitted);
    EXPECT_FALSE(conn_rst_transmitted);
    EXPECT_FALSE(conn_alv_transmitted);
    EXPECT_FALSE(udata_msg_transmitted);
}
