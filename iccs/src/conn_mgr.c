/*
 * ICC, Inter Cell Communication
 *
 * Copyright (c) Retotech AB, 2016-2017
 *
 * Authors:
 *  Jonas Weståker <jonas.westaker@retotech.se>
 *
 * This work is licensed under the terms of the MIT license.  See
 * the COPYING file in the top-level directory.
 */
#include <stddef.h>
#include <string.h>
#include <stdlib.h>

#include "conn_mgr.h"
#include "iccs_protocol.h"
#include "conn_client.h"
#include "msg_transmitter.h"

typedef enum {
    ICCS_CONN_STATE_UNDEFINED,
    ICCS_CONN_STATE_DISCONNECTED,
    ICCS_CONN_STATE_CONNECTING_MASTER,
    ICCS_CONN_STATE_CONNECTING_SLAVE,
    ICCS_CONN_STATE_CONNECTED,
    ICCS_CONN_STATE_DISCONNECTING,
} iccs_conn_state_t;

static uint8_t next_conn_id = 0;

static void
connect(iccs_conn_t *conn);

static void
receive_msg(iccs_conn_t *conn, iccs_gen_hdr_t *msg);

static void
transmit_udata(iccs_conn_t *conn, uint8_t *buf, size_t len);

static void
alive(iccs_conn_t *conn);

static void
disconnect(iccs_conn_t *conn);

/*
struct iccs_conn {
    iccs_conn_state_t state;
    conn_client_t *client;
    msg_transmitter_t *transmitter;
    uint8_t id;
    void (*connect)(iccs_conn_t *conn);
    void (*receive_msg)(iccs_conn_t *conn, iccs_gen_hdr_t *msg);
    void (*transmit_udata)(iccs_conn_t *conn, uint8_t *buf, size_t len);
    void (*alive)(iccs_conn_t *conn);
    void (*disconnect)(iccs_conn_t *conn);
};
*/

static iccs_conn_t conn = {
    ICCS_CONN_STATE_UNDEFINED,
    NULL,
    NULL,
    0,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
};

iccs_conn_t *
init(conn_client_t *client, msg_transmitter_t *transmitter) {
    conn.state = ICCS_CONN_STATE_DISCONNECTED;
    conn.client = client;
    conn.transmitter = transmitter;
    conn.id = next_conn_id++;
    conn.connect = connect;
    conn.receive_msg = receive_msg;
    conn.transmit_udata = transmit_udata;
    conn.alive = alive;
    conn.disconnect = disconnect;

    return &conn;
}

static void
connect(iccs_conn_t *conn) {
    switch (conn->state) {
    case ICCS_CONN_STATE_DISCONNECTED: {
	iccs_conn_req_t conn_req = {
	    .type = ICCS_CONN_REQ,
	    .conn_id = conn->id,
	};
	conn->transmitter->transmit_conn_req(&conn_req);
	conn->state = ICCS_CONN_STATE_CONNECTING_MASTER;
	break;
    }
    case ICCS_CONN_STATE_CONNECTING_MASTER:
    case ICCS_CONN_STATE_CONNECTING_SLAVE:
    case ICCS_CONN_STATE_CONNECTED:
    case ICCS_CONN_STATE_DISCONNECTING:
    case ICCS_CONN_STATE_UNDEFINED:
	break;
    }
}

static void
receive_msg(iccs_conn_t *conn, iccs_gen_hdr_t *msg) {
    (void)msg;
    switch (conn->state) {
    case ICCS_CONN_STATE_DISCONNECTED:
	switch (msg->type) {
	case ICCS_CONN_REQ: {
	    conn->state = ICCS_CONN_STATE_CONNECTING_SLAVE;
	    iccs_conn_rly_t conn_rly = {
		.type = ICCS_CONN_RLY,
		.conn_id = conn->id,
	    };
	    conn->transmitter->transmit_conn_rly(&conn_rly);
	    break;
	}
	default:
	    break;
	}
	break;
    case ICCS_CONN_STATE_CONNECTING_MASTER:
	switch (msg->type) {
	case ICCS_CONN_RLY:
	    conn->state = ICCS_CONN_STATE_CONNECTED;
	    conn->client->connected();
	    break;
	default:
	    break;
	}
	break;
    case ICCS_CONN_STATE_CONNECTING_SLAVE:
	break;
    case ICCS_CONN_STATE_CONNECTED:
	switch (msg->type) {
	case ICCS_CONN_RST:
	    conn->state = ICCS_CONN_STATE_DISCONNECTED;
	    conn->transmitter->transmit_conn_rst((iccs_conn_rst_t *)msg);
	    break;
	default:
	    break;
	}
	break;
    case ICCS_CONN_STATE_DISCONNECTING:
    case ICCS_CONN_STATE_UNDEFINED:
	break;
    }
}

static void
transmit_udata(iccs_conn_t *conn, uint8_t *buf, size_t len) {
    (void)buf;
    (void)len;
    switch (conn->state) {
    case ICCS_CONN_STATE_CONNECTED: {
	iccs_udata_msg_t *udata_msg =
	    (iccs_udata_msg_t *)malloc(sizeof(iccs_udata_msg_t) + len);
	udata_msg->type = ICCS_UDATA_MSG;
	udata_msg->conn_id = conn->id;
	udata_msg->size = len;
	memcpy(&(udata_msg->udata), buf, len);
	conn->transmitter->transmit_udata_msg(udata_msg);
	free(udata_msg);
	break;
    }
    case ICCS_CONN_STATE_DISCONNECTED:
    case ICCS_CONN_STATE_CONNECTING_MASTER:
    case ICCS_CONN_STATE_CONNECTING_SLAVE:
    case ICCS_CONN_STATE_DISCONNECTING:
    case ICCS_CONN_STATE_UNDEFINED:
	break;
    }
}

static void
alive(iccs_conn_t *conn) {
    switch (conn->state) {
    case ICCS_CONN_STATE_CONNECTED: {
	iccs_conn_alv_t conn_alv_msg = {
	    .type = ICCS_CONN_ALV,
	    .conn_id = conn->id,
	};
	conn->transmitter->transmit_conn_alv(&conn_alv_msg);
	break;
    }
    case ICCS_CONN_STATE_DISCONNECTED:
    case ICCS_CONN_STATE_CONNECTING_MASTER:
    case ICCS_CONN_STATE_CONNECTING_SLAVE:
    case ICCS_CONN_STATE_DISCONNECTING:
    case ICCS_CONN_STATE_UNDEFINED:
	break;
    }
}

static void
disconnect(iccs_conn_t *conn) {
    switch (conn->state) {
    case ICCS_CONN_STATE_CONNECTED: {
	conn->state = ICCS_CONN_STATE_DISCONNECTING;
	iccs_conn_rst_t conn_rst_msg = {
	    .type = ICCS_CONN_RST,
	    .conn_id = conn->id,
	};
	conn->transmitter->transmit_conn_rst(&conn_rst_msg);
	break;
    }
    case ICCS_CONN_STATE_DISCONNECTED:
    case ICCS_CONN_STATE_CONNECTING_MASTER:
    case ICCS_CONN_STATE_CONNECTING_SLAVE:
    case ICCS_CONN_STATE_DISCONNECTING:
    case ICCS_CONN_STATE_UNDEFINED:
	break;
    }
}
