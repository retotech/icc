cmake_minimum_required (VERSION 2.6)

include_directories (iccsd ${icc_SOURCE_DIR}/iccs/include)
add_executable (iccspump iccspump.c)
add_executable (iccsd $<TARGET_OBJECTS:iccs> iccsd.c)

include_directories (icctd ${icc_SOURCE_DIR}/icct/include)
add_executable (iccts iccts.c)
add_executable (icctpump icctpump.c)
add_executable (icctdump icctdump.c)
add_executable (icctd icctd.c)
target_link_libraries(icctd icct)
