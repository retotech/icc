# Documentation
## Viewing diagrams
```bash
java -jar <plantuml_download_dir>/plantuml.jar
```
In the file browser that appears, select the diagram you want to view, e.g. `../doc/sim_components.uml`, or `../../doc/components.uml`.

# Building

## For host using host compiler

### From command line
```bash
gcc iccts.c -o iccts
gcc -c ../iccs/src/conn_mgr.c -o conn_mgr.o -I ../iccs/include
gcc -c iccsd.c -o iccsd.o -I ../iccs/include
gcc conn_mgr.o iccsd.o -o iccsd
gcc iccspump.c -o iccspump -I ../iccs/include
```

### Using cmake
```bash
cd ..
mkdir build
cd build
cmake ..
make
```

## For target
### Using cross compiler from command line
TBD

### Using cmake
TBD

# Running
```bash
./iccts &
./iccsd &
./iccshpump icct_conn_req <conn-id>
```
