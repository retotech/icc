/*
 * ICC, Inter Cell Communication
 *
 * Copyright (c) Retotech AB, 2016-2017
 *
 * Authors:
 *  Jonas Weståker <jonas.westaker@retotech.se>
 *
 * This work is licensed under the terms of the MIT license.  See
 * the COPYING file in the top-level directory.
 */
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#include <sys/socket.h>
#include <sys/un.h>

#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <poll.h>

#include <sys/queue.h>

#include "icctd.h"

#include "ivshmem_drv.h"
#include "ivshmem_client.h"

#define SIZE_1MiB (1 << 20)
#define SIZE_4KiB (1 << 12)
#define SIZE_256B (1 << 8)
#define IVSHMEM_SIZE_TX SIZE_4KiB
#define IVSHMEM_SIZE_RX SIZE_4KiB
#define IVSHMEM_SIZE_SHMEM SIZE_1MiB
#define IVSHMEM_SIZE_BAR0 SIZE_256B // 256B

enum pollfds_ixs {
    ICCTD_LISTEN_FD = 0,
    IVSHMEM_INT_FD = 1,
    ICCTC_FD_0 = 2,
};

STAILQ_HEAD(stailhead, entry) head = STAILQ_HEAD_INITIALIZER(head);

struct entry {
    uint8_t *msg;
    size_t len;
    STAILQ_ENTRY(entry) entries;
};

static int
allow_clients_to_connect(const char *sock_path) {
    int fd;
    struct sockaddr_un local;
    fprintf(stdout, "Allow clients to connect on '%s'\n", sock_path);
    if ((fd = socket(AF_UNIX, SOCK_STREAM, 0)) == -1) {
	perror("socket");
	exit(1);
    }

    local.sun_family = AF_UNIX;
    strcpy(local.sun_path, sock_path);
    unlink(local.sun_path);
    size_t len = strlen(local.sun_path) + sizeof(local.sun_family);
    if (bind(fd, (struct sockaddr *)&local, len) == -1) {
	perror("bind");
	exit(1);
    }

    if (listen(fd, 5) == -1) {
	perror("listen");
	exit(1);
    }

    fprintf(stdout, "Listening for clients on fd:%d.\n", fd);
    return fd;
}

static int
wait_for_client_to_connect(int fd) {
    int cfd;
    printf("Waiting for a client to connect on fd:%d.\n", fd);
    struct sockaddr_un remote;
    int t = sizeof(remote);
    if ((cfd = accept(fd, (struct sockaddr *)&remote, &t)) == -1) {
	perror("accept");
	exit(1);
    }

    printf("Accepted client connection on fd:%d.\n", fd);
    return cfd;
}

static uint8_t *
msg_alloc(ivshmem_client_t *ivshmem_client, size_t len) {
    return (uint8_t *)malloc(len);
}

static void
rx_msg(ivshmem_client_t *ivshmem_client, uint8_t *data, size_t len) {
    fprintf(stdout,
	    "Received message (len:%lu, data:'%s')\n", len, (char *)data);
    /* FIXME: write data to client socket before freeing message */
    // Allocate queue element and insert at the tail (last)
    struct entry *last = malloc(sizeof(struct entry));
    STAILQ_INSERT_TAIL(&head, last, entries);
    last->msg = data;
    last->len = len;
}

static ivshmem_client_t ivshmem_client = {
    msg_alloc,
    rx_msg
};

void
usage(char progName[]) {
    fprintf(stderr, "%s [<fileName>]\n", progName);
    fprintf(stderr,
	    "<fileName>\tdevice file providing access to \n"
	    "\t\tvirtual PCI ivshmem device used for Inter Cell Communication\n"
	    "\t\tdefault is /dev/uio0 (provided by uio_ivshmem.ko).\n");
}

int
main(int argc, char *argv[]) {
    char* fileName;
    if (1 == argc) {
	fileName = "/dev/uio0";
    } else if (2 == argc) {
	fileName = argv[1];
    } else {
	usage(argv[0]);
	fprintf(stderr,
		"Expected zero or one argument(s), got %d (", argc -1);
	int ix = 1;
	for (ix = 1; ix <= argc; ix++) {
	    fprintf(stderr, "'%s'", argv[ix]);
	    if (ix < argc) {
		fprintf(stderr, ", ");
	    }
	}
	fprintf(stderr, ")\n");
	return 1;
    }

    /* Open device file */
    int ivshmem_fd = open(fileName, O_RDWR);
    if (-1 == ivshmem_fd) {
	perror(fileName);
    }

    /* Map PCI BAR 0 memory
       (first 256 bytes of first 4KiB (page) in the ivshmem device file) */
    uint8_t *bar0 = mmap(NULL, IVSHMEM_SIZE_BAR0, PROT_READ | PROT_WRITE,
			 MAP_SHARED, ivshmem_fd, 0);
    if (MAP_FAILED == bar0) {
	perror("mmap for bar0 failed");
    }
    fprintf(stdout, "bar0[%p:%p)\n", bar0, bar0 + IVSHMEM_SIZE_BAR0);

    /* Map shmem (how big?)
       (second 4KiB page -> last page) in the ivshmem device file) */
    uint8_t *shmem = mmap(NULL, IVSHMEM_SIZE_SHMEM, PROT_WRITE,
		       MAP_SHARED, ivshmem_fd, SIZE_4KiB);
    if (MAP_FAILED == shmem) {
	perror("mmap for tx area failed");
    }
    fprintf(stdout, "shmem[%p:%p)\n", shmem, shmem + IVSHMEM_SIZE_SHMEM);

    ivshmem_drv_t *ivshmem_drv =
	ivshmem_drv_init(&ivshmem_client, bar0, IVSHMEM_SIZE_BAR0,
			 shmem, IVSHMEM_SIZE_SHMEM/2,
			 shmem + IVSHMEM_SIZE_SHMEM/2, IVSHMEM_SIZE_SHMEM/2);

    struct pollfd fds[ICCTC_FD_0 + 1];
    /* Open UDS to which clients may connect */
    fds[ICCTD_LISTEN_FD].fd = allow_clients_to_connect(SOCK_PATH_ICCTD);
    fds[ICCTD_LISTEN_FD].events = POLLIN;
    fds[IVSHMEM_INT_FD].fd = ivshmem_fd;
    fds[IVSHMEM_INT_FD].events = POLLIN;
    fds[ICCTC_FD_0].fd = -1;
    fds[ICCTC_FD_0].events = 0;

    /* Poll/select shmem device file for interrupts
       and the UDS used for transmitting/receiving
       (iccs protocol messages/raw data)
       from/to a client (iccsd/icctpump) */
    uint8_t tx_buf[1024];
    uint8_t *tx_start = tx_buf;
    size_t tx_size = 0;

    uint8_t *rx_cursor = NULL;
    size_t rx_remainder = 0;

    uint32_t interrupts = 0;

    while (true) {
	int rv = poll(fds, sizeof(fds) / sizeof(fds[0]), 1000);
	//fprintf(stdout, "%d = poll(fds, %ld, 1000)\n",
	//        rv, sizeof(fds)/sizeof(fds[0]));

	switch (rv) {
	case -1:
	    perror("poll");
	    exit(1);
	    break;
	case 0:
	    //fprintf(stdout, "rv:%d - Timeout\n", rv);
	    break;
	default:
	    fprintf(stdout, "%s:%d - %d = poll()\n", __FILE__, __LINE__, rv);
	    /* Check which file descriptor trigged poll/select
	       and take appropriate action,
	       i.e. fragment messages received on the UDS into shmem frames
	       and write them to the shmem tx side or
	       assemble frames received on the shmem rx side into messages
	       and write them to the UDS to which the client is connected. */
	    if (fds[ICCTD_LISTEN_FD].revents & POLLIN) {
		if (-1 == fds[ICCTC_FD_0].fd) {
		    // Client wants to connect on icctd socket
		    fds[ICCTC_FD_0].fd =
			wait_for_client_to_connect(fds[ICCTD_LISTEN_FD].fd);
		    fds[ICCTC_FD_0].events = POLLIN | POLLOUT;
		} else {
		    fprintf(stderr, "Client already connected.\n");
		    exit(2);
		}
	    }

	    if (fds[IVSHMEM_INT_FD].revents & POLLIN) {
		uint32_t buf;
		int n = read(fds[IVSHMEM_INT_FD].fd, &buf, sizeof(buf));
		if (n < 0) {
		    perror("read");
		    exit(3);
		}
		fprintf(stdout,
			"Read %d bytes from fd:%d, buf:%#x\n",
			n, fds[IVSHMEM_INT_FD].fd, buf);
		fprintf(stdout,
			"Interrupt (#%d) received.\n", interrupts++);
		// Either an rx-interrupt was received,
		// indicating that a transmitted
		// message has been received by the receiving cell
		// and the shmem used may be reused,
		// or a tx-interrupt was received,
		// indicating that a message is available for reading
		// from the shmem prior to generating an rx-interrupt
		// to the transmitting cell.
		ivshmem_drv->handle_interrupt(ivshmem_drv);
		if (-1 == fds[ICCTC_FD_0].fd) {
		    fprintf(stdout,
			    "No client connected, discarding any received messages.\n");
		    struct entry *first = STAILQ_FIRST(&head);
		    while (NULL != first) {
			struct entry *next = STAILQ_NEXT(first, entries);
			free(first->msg); // discard message
			free(first); // discard queue element
			first = next;
		    }
		    STAILQ_INIT(&head);
		} else {
		    struct entry *first = STAILQ_FIRST(&head);
		    if (NULL != first) {
			fds[ICCTC_FD_0].events |= POLLOUT;
		    }
		}
	    }

	    if (fds[ICCTC_FD_0].revents & POLLIN) {
		// Data written by client on icctc socket
		//fprintf(stdout,
		//	"Received data on icctc fd:%d\n", fds[ICCTC_FD_0].fd);
		if (-1 == fds[ICCTC_FD_0].fd) {
		    fprintf(stderr,
			    "%s:%d - No client connected yet.\n",
			    __FILE__, __LINE__);
		    break;
		}

		// Read message (string) from icctc socket
		int n = recv(fds[ICCTC_FD_0].fd, tx_start,
			     sizeof(tx_buf)/sizeof(tx_buf[0]) - tx_size, 0);
		if (-1 == n) {
		    perror("recv");
		    exit(1);
		}
		if (0 == n) {
		    fprintf(stdout,
			    "%s:%d - Client disconnected from icctc socket.\n",
			    __FILE__, __LINE__);
		    fds[ICCTC_FD_0].fd = -1;
		    fds[ICCTC_FD_0].events = 0;
		    break;
		}

		tx_size += n;
		size_t len;
		tx_start = tx_buf;
		while (tx_size > (len = strnlen((char *)tx_start, tx_size))) {
		    // At least one (NUL-terminated) string in tx_buf
		    fprintf(stdout,
			    "%s:%d - Received '%s'\n",
			    __FILE__, __LINE__, (char *)tx_start);
		    ivshmem_drv->tx_msg(ivshmem_drv, tx_start, len + 1);
		    tx_start += len + 1;
		    tx_size -= len + 1;
		}
		// Move any remaining data recv:ed
		// (not yet NUL-terminated string)
		// to start of tx_buf
		memcpy(tx_buf, tx_start, tx_size);
		tx_start -= tx_size;
	    }

	    if (fds[ICCTC_FD_0].revents & POLLOUT) {
		fprintf(stdout, "%s:%d - POLLOUT\n", __FILE__, __LINE__);
		if (NULL == rx_cursor) {
		    struct entry *first = STAILQ_FIRST(&head);
		    if (NULL != first) {
			STAILQ_REMOVE_HEAD(&head, entries);
			rx_cursor = first->msg;
			rx_remainder = first->len;
			free(first);
		    } else {
			fds[ICCTC_FD_0].events &= ~POLLOUT;
			break;
		    }
		}
		ssize_t n =
		    send(fds[ICCTC_FD_0].fd, rx_cursor, rx_remainder, 0);
		fprintf(stdout, "%s:%d - %d = send()\n", __FILE__, __LINE__, n);
		if (0 > n) {
		    perror("send");
		    exit(n);
		}
		if (rx_remainder > n) {
		    rx_cursor += n;
		    rx_remainder -= n;
		} else {
		    rx_cursor = NULL;
		    rx_remainder = 0;
		}
	    }

	    break;
	} // default:
    } // while (true)

    return 0;
}
