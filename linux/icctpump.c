/*
 * ICC, Inter Cell Communication
 *
 * Copyright (c) Retotech AB, 2016-2017
 *
 * Authors:
 *  Jonas Weståker <jonas.westaker@retotech.se>
 *
 * This work is licensed under the terms of the MIT license.  See
 * the COPYING file in the top-level directory.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <poll.h>
#include <stdbool.h>
#include <unistd.h>

#include "icctd.h"

int main(int argc, char *argv[]) 
{
    int s, t, len;
    struct sockaddr_un remote;
    char str[100];
    if ((s = socket(AF_UNIX, SOCK_STREAM, 0)) == -1) {
        perror("socket");
        exit(1);
    }
    remote.sun_family = AF_UNIX;
    strcpy(remote.sun_path, SOCK_PATH_ICCTD);
    len = strlen(remote.sun_path) + sizeof(remote.sun_family);
    if (connect(s, (struct sockaddr *)&remote, len) == -1) {
        perror("connect");
        exit(1);
    }
    fprintf(stdout, "%s:%d - %s Connected to %s.\n",
	    __FILE__, __LINE__, argv[0], SOCK_PATH_ICCTD);

    struct pollfd ufds[] = {
	{
	    .fd = s,
	    .events = POLLIN | POLLOUT
	}
    };

    bool done = false;
    while (! done) {
	int rv = poll(ufds, sizeof(ufds) / sizeof(ufds[0]), 1000);
	//fprintf(stdout,
	//	"%s:%d - %d = poll();\n",
	//	__FILE__, __LINE__, rv);
	switch (rv) {
	case -1:
	    perror("poll");
	    exit(1);
	    break;
	case 0:
	    //printf("rv:%d - Timeout\n", rv);
	    break;
	default: {
	    printf("rv: %d\n", rv);
	    if (ufds[0].revents & POLLIN) {
		fprintf(stdout,
			"%s:%d - Ready to recv from ufds[0].fd:%d\n",
			__FILE__, __LINE__, ufds[0].fd);
		char buf[80];
		ssize_t bytes = recv(ufds[0].fd,
				     &buf, sizeof(buf)/sizeof(buf[0]), 0);

		if (0 > bytes) {
		    perror("recv");
		    exit(bytes);
		}
		if (0 == bytes) {
		    fprintf(stdout,
			    "%s:%d - Peer hung up\n",
			    __FILE__, __LINE__);
		    ufds[0].fd = -1;
		    exit(1);
		    break;
		}
		fprintf(stdout,
			"%s:%d - %ld = recv() ('%s') from ufds[0].fd:%d.\n",
			__FILE__, __LINE__, bytes, buf, ufds[0].fd);
		done = true;
	    } // if (udfs[0].revents & POLLIN)
	    else if (ufds[0].revents & POLLOUT) {
		fprintf(stdout,
			"%s:%d - Ready to send to ufds[0].fd:%d.\n",
			__FILE__, __LINE__, ufds[0].fd);
		char buf[] = "Hello from icctpump";
		size_t len = strlen(buf) + 1;
		ssize_t n = send(ufds[0].fd, buf, sizeof(buf), 0);
		if (0 > n) {
		    perror("send");
		    exit(1);
		}
		fprintf(stdout,
			"%s:%d - %d = send() ('%s') to ufds[0].fd:%d\n",
			__FILE__, __LINE__, n, buf, ufds[0].fd);
		ufds[0].events &= ~POLLOUT;
	    }
	    break;
	} // default:
	} // switch (rv)
    }  // while (! done)
    /*
    fprintf(stdout,
	    "%s:%d - Closing ufds[0].fd:%d\n"
	    __FILE__, __LINE__, ufds[0].fd);
    */
    fprintf(stdout, "%s:%d - Done!\n", __FILE__, __LINE__);
    close(s);
    return 0;
}
