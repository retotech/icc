/*
 * ICC, Inter Cell Communication
 *
 * Copyright (c) Retotech AB, 2016-2017
 *
 * Authors:
 *  Jonas Weståker <jonas.westaker@retotech.se>
 *
 * This work is licensed under the terms of the MIT license.  See
 * the COPYING file in the top-level directory.
 */
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <poll.h>
#include <stdbool.h>
#include <unistd.h>

#include "iccts.h"
#include "iccs_protocol.h"

#define ICCSPUMP_CMD_ICCS_CONN_REQ_NAME "icct_conn_req"
#define ICCSPUMP_CMD_ICCS_CONN_REQ_USAGE "icct_conn_req <conn_id>"

typedef struct {
    char * name;
    char * usage;
    int argc;
} iccspump_cmd_t;


static iccspump_cmd_t iccspump_cmds[] = {
    {
	ICCSPUMP_CMD_ICCS_CONN_REQ_NAME,
	ICCSPUMP_CMD_ICCS_CONN_REQ_USAGE,
	1
    }
};

int main(int argc, char *argv[]) 
{
    int ix;
    if (2 > argc) {
	for (ix = 0;
	     ix < sizeof(iccspump_cmds) / sizeof(iccspump_cmds[0]);
	     ix++)
	{
	    fprintf(stderr, "%s\n", iccspump_cmds[ix].usage);
	}
	return 1;
    }

    bool found = false;
    for (ix = 0;
	 ix < sizeof(iccspump_cmds) / sizeof(iccspump_cmds[0]);
	 ix++)
    {
	if (! strcmp(iccspump_cmds[ix].name, argv[1])) {
	    if (argc - 2 != iccspump_cmds[ix].argc) {
		fprintf(stderr, "%s\n", iccspump_cmds[ix].usage);
		return 1;
	    }
	    found = true;
	    break;
	}
    }

    if (!found) {
	fprintf(stderr, "Command '%s' not found. Available commands are:\n", argv[1]);
	for (ix = 0;
	     ix < sizeof(iccspump_cmds) / sizeof(iccspump_cmds[0]);
	     ix++)
	{
	    fprintf(stderr, "\t%s\n", iccspump_cmds[ix].name);
	}
	return 1;
    }

    // Parse connection-id argument
    errno = 0;
    char *endptr;
    unsigned long int conn_id = strtoul(argv[2], &endptr, 0);

    if (((ULONG_MAX == conn_id) || (0 == conn_id)) && (0 != errno)) {
	perror("strtoul");
	exit(1);
    }

    if (argv[2] == endptr) {
	fprintf(stderr,
		"Illegal <conn-id> argument '%s' (not able to parse to a valid number of type uint8_t).\n", argv[2]);
	exit(1);
    }
    if ('\0' != *endptr) {
	fprintf(stderr, "Illegal character(s) '%s' in <conn-id> argument '%s'.\n", endptr, argv[2]);
	exit(1);
    }

    if (conn_id > 0xff) {
	fprintf(stderr, "<conn-id> argument %lu is too large (not able to fit into uint8_t).\n", conn_id);
	exit(1);
    }

    printf("Running command '%s %lu'\n", iccspump_cmds[ix].name, conn_id);

    int s;
    size_t len;
    struct sockaddr_un remote;
    char str[100];

    if ((s = socket(AF_UNIX, SOCK_STREAM, 0)) == -1) {
        perror("socket");
        exit(1);
    }
    printf("Trying to connect to %s...\n", SOCK_PATH_ICCTS);
    remote.sun_family = AF_UNIX;
    strcpy(remote.sun_path, SOCK_PATH_ICCTS);
    len = strlen(remote.sun_path) + sizeof(remote.sun_family);
    if (connect(s, (struct sockaddr *)&remote, len) == -1) {
        perror("connect");
        exit(1);
    }
    printf("%s - Connected.\n", argv[0]);

    if (!strcmp(argv[1], ICCSPUMP_CMD_ICCS_CONN_REQ_NAME)) {
	iccs_conn_req_t iccs_conn_req_msg = {
	    .type = ICCS_CONN_REQ,
	    .conn_id = (0xff & conn_id)
	};
	if (send(s, &iccs_conn_req_msg, sizeof(iccs_conn_req_msg), 0) == -1) {
	    perror("send");
	    exit(1);
	}
	printf("Sent %s message\n", ICCS_CONN_REQ_NAME);
    }

    close(s);

    return 0;
}
