/*
 * ICC, Inter Cell Communication
 *
 * Copyright (c) Retotech AB, 2016-2017
 *
 * Authors:
 *  Jonas Weståker <jonas.westaker@retotech.se>
 *
 * This work is licensed under the terms of the MIT license.  See
 * the COPYING file in the top-level directory.
 */
#ifndef ICCTD_H
#define ICCTD_H

#define SOCK_PATH_ICCTD "/var/run/UDS_icctd"

#endif /* #ifndef ICCTD_H */
