/*
 * ICC, Inter Cell Communication
 *
 * Copyright (c) Retotech AB, 2016-2017
 *
 * Authors:
 *  Jonas Weståker <jonas.westaker@retotech.se>
 *
 * This work is licensed under the terms of the MIT license.  See
 * the COPYING file in the top-level directory.
 */
#ifndef ICCTS_H
#define ICCTS_H

#define SOCK_PATH_ICCTS "UDS_iccts"

#endif /* #ifndef ICCTS_H */
