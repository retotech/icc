/*
 * ICC, Inter Cell Communication
 *
 * Copyright (c) Retotech AB, 2016-2017
 *
 * Authors:
 *  Jonas Weståker <jonas.westaker@retotech.se>
 *
 * This work is licensed under the terms of the MIT license.  See
 * the COPYING file in the top-level directory.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <poll.h>
#include <stdbool.h>
#include <unistd.h>
#include <sys/queue.h>

#include "icctd.h"
#include "iccs_protocol.h"
#include "conn_client.h"
#include "conn_mgr.h"
#include "msg_transmitter.h"

#define STATE_DISCONNECTED_ID 0
#define STATE_CONNECTING_SLAVE_ID 1
#define STATE_CONNECTING_MASTER_ID 2
#define STATE_CONNECTED_ID 3
#define STATE_DISCONNECTING_ID 4

#define STATE_DISCONNECTED_NAME "DISCONNECTED"
#define STATE_CONNECTING_SLAVE_NAME "CONNECTING_SLAVE"
#define STATE_CONNECTING_MASTER_NAME "CONNECTING_MASTER"
#define STATE_CONNECTED_NAME "CONNECTED"
#define STATE_DISCONNECTING_NAME "DISCONNECTING"

static char *stateNames[] = {
    STATE_DISCONNECTED_NAME,
    STATE_CONNECTING_SLAVE_NAME,
    STATE_CONNECTING_MASTER_NAME,
    STATE_CONNECTED_NAME,
    STATE_DISCONNECTING_NAME
};

static void
connected(void) {
    printf("Connected!\n");
}

static void
deliver(void *msg, size_t len) {
    printf("Delivering message of length:%lu\n", len);
}

static void
disconnected(void) {
    printf("Disconnected!\n");
}

static conn_client_t client = {
    .connected = connected,
    .msg_alloc = malloc,
    .msg_deliver = deliver,
    .msg_free = free,
    .disconnected = disconnected,
};

STAILQ_HEAD(txq_head, txq_entry) head = STAILQ_HEAD_INITIALIZER(head);

struct txq_head *headp;

struct txq_entry {
    STAILQ_ENTRY(txq_entry) txq_entries;
    size_t len;
    uint8_t buf[];
};

static void
enqueue_conn_req(iccs_conn_req_t *msg) {
    printf("%s:%s()\n", __FILE__, __func__);
    struct txq_entry *entry =
	malloc(sizeof(struct txq_entry) + sizeof(*msg));
    STAILQ_INSERT_TAIL(&head, entry, txq_entries);
    entry->len = sizeof(*msg);
    memcpy(entry->buf, msg, sizeof(*msg));
}

static void
enqueue_conn_rly(iccs_conn_rly_t *msg) {
    printf("%s:%s() - starting...\n", __FILE__, __func__);
    struct txq_entry *entry =
	malloc(sizeof(struct txq_entry) + sizeof(*msg));
    STAILQ_INSERT_TAIL(&head, entry, txq_entries);
    entry->len = sizeof(*msg);
    memcpy(&(entry->buf), msg, sizeof(*msg));
    printf("%s:%s() - done...\n", __FILE__, __func__);

}

static void
enqueue_conn_alv(iccs_conn_alv_t *msg) {
    printf("%s:%s()\n", __FILE__, __func__);
    struct txq_entry *entry =
	malloc(sizeof(struct txq_entry) + sizeof(*msg));
    STAILQ_INSERT_TAIL(&head, entry, txq_entries);
    entry->len = sizeof(*msg);
    memcpy(&(entry->buf), msg, sizeof(*msg));
}

static void
enqueue_conn_rst(iccs_conn_rst_t *msg) {
    printf("%s:%s()\n", __FILE__, __func__);
    struct txq_entry *entry =
	malloc(sizeof(struct txq_entry) + sizeof(*msg));
    STAILQ_INSERT_TAIL(&head, entry, txq_entries);
    entry->len = sizeof(*msg);
    memcpy(&(entry->buf), msg, sizeof(*msg));
}

static void
enqueue_udata_msg(iccs_udata_msg_t *msg) {
    printf("%s:%s()\n", __FILE__, __func__);
    struct txq_entry *entry =
	malloc(sizeof(struct txq_entry) + sizeof(*msg));
    STAILQ_INSERT_TAIL(&head, entry, txq_entries);
    entry->len = sizeof(*msg);
    memcpy(&(entry->buf), msg, sizeof(*msg));
}

static msg_transmitter_t transmitter = {
    .transmit_conn_req = enqueue_conn_req,
    .transmit_conn_rly = enqueue_conn_rly,
    .transmit_conn_alv = enqueue_conn_alv,
    .transmit_conn_rst = enqueue_conn_rst,
    .transmit_udata_msg = enqueue_udata_msg,
};

static char*
stateName(unsigned int stateId) {
    return stateNames[stateId];
}

int main(int argc, char *argv[]) 
{
    int s, t, len;
    struct sockaddr_un remote;
    char str[100];
    if ((s = socket(AF_UNIX, SOCK_STREAM, 0)) == -1) {
        perror("socket");
        exit(1);
    }
    printf("Trying to connect...\n");
    remote.sun_family = AF_UNIX;
    strcpy(remote.sun_path, SOCK_PATH_ICCTD);
    len = strlen(remote.sun_path) + sizeof(remote.sun_family);
    if (connect(s, (struct sockaddr *)&remote, len) == -1) {
        perror("connect");
        exit(1);
    }
    printf("%s - Connected.\n", argv[0]);

    iccs_conn_t *conn = init(&client, &transmitter);

    struct pollfd ufds[] = {
	{
	    .fd = s,
	    .events = POLLIN// | POLLOUT
	}
    };

    unsigned int state = STATE_DISCONNECTED_ID;

    STAILQ_INIT(&head);

    while (true) {
	int rv = poll(ufds, sizeof(ufds) / sizeof(ufds[0]), 1000);
	switch (rv) {
	case -1:
	    perror("poll");
	    exit(1);
	    break;
	case 0:
	    //printf("rv:%d - Timeout\n", rv);
	    break;
	default: {
	    printf("rv: %d\n", rv);
	    if (ufds[0].revents & POLLIN) {
		printf("Ready to read from ufds[0].fd:%d\n", ufds[0].fd);
		iccs_gen_hdr_t hdr;
		ssize_t bytes = recv(ufds[0].fd, &hdr, sizeof(hdr), 0);
		if (0 == bytes) {
		    printf("Peer hung up\n");
		    ufds[0].fd = -1;
		    exit(1);
		    break;
		}

		printf("Received %s in state %s\n",
		       iccsMsgName(hdr.type), stateName(state));

		switch (hdr.type) {
		case ICCS_CONN_REQ:
		case ICCS_CONN_RLY:
		case ICCS_CONN_RST:
		case ICCS_CONN_ALV:
		    conn->receive_msg(conn, &hdr);
		    break;
		case ICCS_UDATA_MSG: {
		    iccs_udata_msg_t *msg = (iccs_udata_msg_t *)&hdr;
		    bytes = recv(ufds[0].fd, &(msg->size),
				 sizeof(msg->size), 0);
		    uint8_t *udata = client.msg_alloc(msg->size);
		    bytes = recv(ufds[0].fd, udata, msg->size, 0);
		    conn->receive_msg(conn, (iccs_gen_hdr_t *)msg);
		    client.msg_free(udata);
		    break;
		}
		default: {
		    fprintf(stderr,
			    "Received unknown iccs message (type:%d)\n",
			    hdr.type);
		    close(s);
		    exit(1);
		    break;
		}
		} // switch (hdr.type)
	    } // if (udfs[0].revents & POLLIN)

	    if (ufds[0].revents & POLLOUT) {
		printf("Ready to write to ufds[0]\n");
		while (! STAILQ_EMPTY(&head)) {
		    struct txq_entry *txq_entry = STAILQ_FIRST(&head);
		    STAILQ_REMOVE_HEAD(&head, txq_entries);
		    size_t sent = 0;
		    size_t remainder = txq_entry->len;
		    uint8_t *cursor = &(txq_entry->buf[0]);
		    do {
			if (0 > (sent = send(ufds[0].fd, cursor,
					     remainder, 0)))
			{
			    perror("send");
			    break;
			}
			*cursor += sent;
			remainder -= sent;
		    } while (remainder);
		    free(txq_entry);
		}
		printf("Done writing to ufds[0]\n");
		ufds[0].events &= ~POLLOUT;
	    }
	    break;
	} // default:
	} // switch (rv)

	if (!STAILQ_EMPTY(&head)) {
	    // Wait until fd ready to send()
	    ufds[0].events |= POLLOUT;
	}
    }  // while (true)
    close(s);
    return 0;
}
