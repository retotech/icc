*** Settings ***
Test Setup        Setup iccsd linux module test environment
Test Teardown     Teardown iccsd linux module test environment
Library           SSHLibrary
Library           Process

*** Variables ***
${ICC-PATH}       ${EMPTY}
${ICCTS-PATH}     ${ICC-PATH}/linux
${ICCSD-PATH}     ${ICC-PATH}/linux
${ICCSPUMP-PATH}    ${ICC-PATH}/linux

*** Test Cases ***
Send iccs_conn_req to disconnected iccsd
    ${result}=    Run Process    ${ICCSPUMP-PATH}${/}iccspump    icct_conn_req    1    cwd=${ICCSPUMP-PATH}
    Log    ${result.stdout}
    Log    ${result.stderr}
    Should Be Equal As Integers    ${result.rc}    0

Send iccs_conn_rly to disconnected iccsd
    ${result}=    Run Process    ${ICCSPUMP-PATH}${/}iccspump    icct_conn_rly    1    cwd=${ICCSPUMP-PATH}
    Log    ${result.stdout}
    Log    ${result.stderr}
    Should Be Equal As Integers    ${result.rc}    1

Send iccs_conn_rst to disconnected iccsd
    ${result}=    Run Process    ${ICCSPUMP-PATH}${/}iccspump    icct_conn_rst    1    cwd=${ICCSPUMP-PATH}
    Log    ${result.stdout}
    Log    ${result.stderr}
    Should Be Equal As Integers    ${result.rc}    1

Send iccs_conn_alv to disconnected iccsd
    ${result}=    Run Process    ${ICCSPUMP-PATH}${/}iccspump    icct_conn_alv    1    cwd=${ICCSPUMP-PATH}
    Log    ${result.stdout}
    Log    ${result.stderr}
    Should Be Equal As Integers    ${result.rc}    1

Send iccs_udata_msg to disconnected iccsd
    ${result}=    Run Process    ${ICCSPUMP-PATH}${/}iccspump    icct_udata_msg    1    1234    cwd=${ICCSPUMP-PATH}
    Log    ${result.stdout}
    Log    ${result.stderr}
    Should Be Equal As Integers    ${result.rc}    1

*** Keywords ***
Setup iccsd linux module test environment
    Start iccts
    Start iccsd
    [Teardown]

Start iccts
    Start Process    ${ICCTS-PATH}${/}iccts    cwd=${ICCTS-PATH}    alias=iccts

Start iccsd
    Start Process    ${ICCSD-PATH}${/}iccsd    cwd=${ICCSD-PATH}    alias=iccsd

Teardown iccsd linux module test environment
    Stop iccsd
    Stop iccts

Stop iccsd
    Terminate Process    iccsd

Stop iccts
    Terminate Process    iccts
