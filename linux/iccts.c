/*
 * ICC, Inter Cell Communication
 *
 * Copyright (c) Retotech AB, 2016-2017
 *
 * Authors:
 *  Jonas Weståker <jonas.westaker@retotech.se>
 *
 * This work is licensed under the terms of the MIT license.  See
 * the COPYING file in the top-level directory.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>
#include <poll.h>
#include <stdbool.h>

#include "icctd.h"
#include "iccts.h"

enum ufds_ix {
    ICCTD_FD_IX,
    ICCTD_FD_1_IX,
    ICCTS_FD_IX,
    ICCTS_FD_1_IX
};

static int
allow_clients_to_connect(const char *sock_path) {
    int fd;
    struct sockaddr_un local;
    if ((fd = socket(AF_UNIX, SOCK_STREAM, 0)) == -1) {
	perror("socket");
	exit(1);
    }

    local.sun_family = AF_UNIX;
    strcpy(local.sun_path, sock_path);
    unlink(local.sun_path);
    size_t len = strlen(local.sun_path) + sizeof(local.sun_family);
    if (bind(fd, (struct sockaddr *)&local, len) == -1) {
	perror("bind");
	exit(1);
    }

    if (listen(fd, 5) == -1) {
	perror("listen");
	exit(1);
    }
    return fd;
}

static int
wait_for_client_to_connect(int fd) {
    int cfd;
    printf("Waiting for a client to connect on fd:%d\n", fd);
    struct sockaddr_un remote;
    int t = sizeof(remote);
    if ((cfd = accept(fd, (struct sockaddr *)&remote, &t)) == -1) {
	perror("accept");
	exit(1);
    }

    printf("Accepted client connection on fd:%d.\n", fd);
    return cfd;
}


int main(int argc, char *argv[]) 
{
    struct pollfd ufds[] = {
	{
	    .fd = -1,
	    .events = POLLIN
	},
	{
	    .fd = -1,
	    .events = POLLIN
	},
	{
	    .fd = -1,
	    .events = POLLIN
	},
	{
	    .fd = -1,
	    .events = POLLIN
	}
    };

    ufds[ICCTD_FD_IX].fd = allow_clients_to_connect(SOCK_PATH_ICCTD);

    for(;;) {

	ufds[ICCTD_FD_1_IX].fd =
	    wait_for_client_to_connect(ufds[ICCTD_FD_IX].fd);

	ufds[ICCTS_FD_IX].fd =
	    allow_clients_to_connect(SOCK_PATH_ICCTS);

	while (true) {
	    int rv = poll(ufds, sizeof(ufds) / sizeof(ufds[0]), 1000);
	    switch (rv) {
	    case -1:
		perror("poll");
		exit(1);
		break;
	    case 0:
		//printf(rv:%d - Timeout\n", rv);
		break;
	    default:
		printf("rv:%d\n", rv);
		if (ufds[ICCTD_FD_IX].revents & POLLIN) {
		    printf("Accepting client connection on %s ufds[%d].fd:%d\n",
			   SOCK_PATH_ICCTD, ICCTD_FD_IX, ufds[ICCTD_FD_IX].fd);

		    ufds[ICCTD_FD_1_IX].fd =
			wait_for_client_to_connect(ufds[ICCTD_FD_IX].fd);

		    ufds[ICCTS_FD_IX].fd =
			allow_clients_to_connect(SOCK_PATH_ICCTS);
		}
		if (ufds[ICCTD_FD_1_IX].revents & POLLIN) {
		    printf("%s:%d - Ready to recv on ufds[%d].fd:%d\n",
			   __FILE__, __LINE__,
			   ICCTD_FD_1_IX, ufds[ICCTD_FD_1_IX].fd);
		    bool done = false;
		    char buf[100];
		    do {
			int n = recv(ufds[ICCTD_FD_1_IX].fd, buf, sizeof(buf), 0);
			if (n < 0) {
			    perror("recv");
			    done = true;
			} else if (0 == n) {
			    printf("%s:%d - icctd client hung up\n",
				   __FILE__, __LINE__);
			    close(ufds[ICCTD_FD_1_IX].fd);
			    ufds[ICCTD_FD_1_IX].fd = -1;
			    close(ufds[ICCTS_FD_IX].fd);
			    ufds[ICCTS_FD_IX].fd = -1;
			    break;
			}
		    } while (!done);
		}
		if (ufds[ICCTS_FD_IX].revents & POLLIN) {
		    ufds[ICCTS_FD_1_IX].fd =
			wait_for_client_to_connect(ufds[ICCTS_FD_IX].fd);
		}
		if (ufds[ICCTS_FD_1_IX].revents & POLLIN) {
		    printf("%s:%d - Ready to recv on ufds[%d].fd:%d\n",
			   __FILE__, __LINE__,
			   ICCTS_FD_1_IX, ufds[ICCTS_FD_1_IX].fd);
		    int done = 0;
		    char str[100];
		    do {
			int n = recv(ufds[ICCTS_FD_1_IX].fd, str, sizeof(str), 0);
			if (n <= 0) {
			    if (n < 0) perror("recv");
			    done = 1;
			    if (0 == n) {
				printf("iccts client hung up\n");
				close(ufds[ICCTS_FD_1_IX].fd);
				ufds[ICCTS_FD_1_IX].fd = -1;
				break;
			    }
			}
			printf("recv:ed %d bytes on udfs[%d].fd:%d\n",
			       n, ICCTS_FD_1_IX, ufds[ICCTS_FD_1_IX].fd);
			if (-1 != ufds[ICCTD_FD_1_IX].fd) {
			    if (!done) {
				if (send(ufds[ICCTD_FD_1_IX].fd, str, n, 0) < 0) {
				    perror("send");
				    done = 1;
				}
				printf("send %d bytes on ufds[%d].fd:%d\n",
				       n, ICCTD_FD_1_IX, ufds[ICCTD_FD_1_IX].fd);
			    }
			}
		    } while (!done);
		}
		break;
	    }
	}
        close(ufds[ICCTD_FD_1_IX].fd);
    }
    return 0;
}
